<?php

namespace app\managers;


use app\dto\ReportDto;
use app\utils\WordHelper;

class ReportManager
{
    /** @var WordHelper */
    private $wordHelper;

    /**
     * ReportManager constructor.
     *
     * @param WordHelper $wordHelper
     */
    public function __construct(WordHelper $wordHelper)
    {
        $this->wordHelper = $wordHelper;
    }

    /**
     * @param ReportDto $reportDto
     *
     * @return string
     */
    public function createReport(ReportDto $reportDto): string
    {
        $renderHtml = \Yii::$app->controller->renderPartial('report', ['reportDto'=>$reportDto]);
        return $this->wordHelper->generateReport($renderHtml);
    }
}