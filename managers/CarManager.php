<?php

namespace app\managers;


use app\entity\car\Car;
use app\entity\driver\Driver;
use app\utils\DateHelper;
use DateTime;

class CarManager
{
    /** Расход топлива на 1 км  (10 / 100) = 0.1 */
    const FUEL_CONSUMPTION_ON_KM = 0.1;
    /** Начачльный процент на поломку */
    const BREAKDOWN_RATIO_DEFAULT = 0.5;
    /** Процент поломки увелиыивающийся на каждые 1000 км */
    const BREAKDOWN_RATIO_ADD = 1;
    /** Дистанция через которую следует увеличивать коэффициент процента поломки */
    const DISTANCE_TO_INCREMENT_BREAKDOWN_RATIO = 1000;
    /** Средняя дистанция одного маршрута */
    const DISTANCE_SINGLE_ROUTE = 7;
    /** Среднее количество заказов за день */
    const COUNT_ORDER_ON_DAY = 10;
    /** Количество дней на починку авто */
    const COUNT_DAYS_AUTO_FIX = 3;

    /** @var DateHelper */
    private $dateHelper;

    /**
     * CarManager constructor.
     *
     * @param DateHelper $dateHelper
     */
    public function __construct(DateHelper $dateHelper)
    {
        $this->dateHelper = $dateHelper;
    }

    /**
     * Получить расход топлива на дистанцию
     *
     * @param Car    $car
     * @param Driver $driver
     * @param int    $distance
     *
     * @return float|int
     */
    public function getFuelConsumptionByDistance(Car $car, Driver $driver, int $distance)
    {
        return $this->getFuelConsumption($car, $driver) * $distance;
    }

    /**
     * Получить коэффициент расход топлива на км в зависимости от авто и водителя
     *
     * @param Car    $car
     * @param Driver $driver
     *
     * @return float|int
     */
    private function getFuelConsumption(Car $car, Driver $driver)
    {
        return static::FUEL_CONSUMPTION_ON_KM * $car->getEconomicalCoefficient() * $driver->getEconomicalCoefficient();
    }

    /**
     * Высчитывает коэффициент поломки в зависимости от авто
     *
     * @param Car $car
     *
     * @return float
     */
    public function getBreakdownRatio(Car $car): float
    {
        $reakdownRatio = (static::BREAKDOWN_RATIO_DEFAULT + $this->getBreakdownRatioByDistance($car->getKm())) * $car->getBreakdownRatio();
        return $reakdownRatio > 100 ? 100 : $reakdownRatio;
    }

    /**
     * Высчитывает коэфициент в зависимости от пройденной дистанции
     *
     * @param int $distance
     *
     * @return int
     */
    private function getBreakdownRatioByDistance(int $distance): int
    {
        return (int)($distance / static::DISTANCE_TO_INCREMENT_BREAKDOWN_RATIO);
    }

    /**
     * Получить количество выполненных заказов за день в зависимости от водителя
     *
     * @param Driver $driver
     *
     * @return int
     */
    public function getCountOrder(Driver $driver): int
    {
        return (int)($driver->getOrderCoefficient() * static::COUNT_ORDER_ON_DAY);
    }

    /**
     * Расстояние пройденное в зависимости от количчества заказов
     *
     * @param int $countOrder
     *
     * @return int
     */
    public function getDistance(int $countOrder): int
    {
        return static::DISTANCE_SINGLE_ROUTE * $countOrder;
    }

    /**
     * Вычисляем сломалась ли машина, также если она сломалась указываем дату поломки
     *
     * @param Car      $car
     * @param DateTime $currentDate
     */
    public function execAndSetCarBrokeDown(Car $car, DateTime $currentDate)
    {
        if ($this->execCarBrokeDown($car)) {
            $car->setBrokenAt($currentDate);
        }
    }

    /**
     * Проверка сломан ли сейчас авто, если авто был сломан и простоял уже более 3 дней, тогда устанвливаем дату поломки null
     *
     * @param Car      $car
     * @param DateTime $currentDate
     *
     * @return bool
     */
    public function carIsBroken(Car $car, DateTime $currentDate): bool
    {
        if($car->getBrokenAt() === null){
            return false;
        }

        if($this->dateHelper->differenceCountDaysBetweenBrokenAndCurrantDate($car->getBrokenAt(), $currentDate) >= static::COUNT_DAYS_AUTO_FIX){
            $car->setBrokenAt(null);
            return false;
        }

        return true;
    }

    /**
     * Вычисляем сломалась ли машина
     *
     * @param Car $car
     *
     * @return bool
     */
    private function execCarBrokeDown(Car $car): bool
    {
        return mt_rand($this->getBreakdownRatio($car), 100) === 100;
    }

    /**
     * Увеличение дистанции авто
     *
     * @param Car $car
     * @param int $km
     */
    public function addDistance(Car $car, int $km)
    {
        $car->addKm($km);
    }

}