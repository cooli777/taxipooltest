<?php

namespace app\facades;


use app\entity\car\Car;
use app\entity\driver\Driver;
use app\entity\report\ReportDaily;
use app\factories\ReportFactory;
use app\managers\CarManager;
use DateTime;

class CarWorkDayFacade
{
    /** @var CarManager */
    private $carManager;
    /** @var ReportFactory */
    private $reportFactory;

    /**
     * CarWorkDayFacade constructor.
     *
     * @param CarManager    $carManager
     * @param ReportFactory $reportFactory
     */
    public function __construct(
        CarManager $carManager,
        ReportFactory $reportFactory
    )
    {
        $this->carManager    = $carManager;
        $this->reportFactory = $reportFactory;
    }

    /**
     * Эмулирует дневную работу авто, возвращает отчет о работе
     *
     * @param Car      $car
     * @param Driver   $driver
     * @param DateTime $currentDate
     *
     * @return ReportDaily
     */
    public function exec(Car $car, Driver $driver, DateTime $currentDate): ReportDaily
    {

        $reportDaily = $this->reportFactory->createForCarAndDriver($car, $driver);
        $this->carManager->execAndSetCarBrokeDown($car, $currentDate);
        $reportDaily->carIsBroken = $this->carManager->carIsBroken($car, $currentDate);
        $reportDaily->probabilityFailure = $this->carManager->getBreakdownRatio($car);

        if ($reportDaily->carIsBroken) {
            $reportDaily->carIsBrokenToday = true;
            return $reportDaily;
        }

        $reportDaily->countOrder              = $this->carManager->getCountOrder($driver);
        $reportDaily->carCountTraveledKmToday = $this->carManager->getDistance($reportDaily->countOrder);
        $this->carManager->addDistance($car, $reportDaily->carCountTraveledKmToday);
        $reportDaily->carCountTraveledKmAll = $car->getKm();
        $reportDaily->fuelConsumption       = $this->carManager->getFuelConsumptionByDistance($car, $driver, $reportDaily->carCountTraveledKmToday);

        return $reportDaily;
    }
}