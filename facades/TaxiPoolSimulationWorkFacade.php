<?php

namespace app\facades;


use app\entity\car\Car;
use app\entity\driver\Driver;
use app\entity\report\Report;
use app\entity\report\ReportDaily;
use app\entity\TaxiPool;
use app\factories\ReportFactory;
use app\managers\CarManager;
use app\utils\DateHelper;
use DateTime;

class TaxiPoolSimulationWorkFacade
{
    /** @var int значение по умолчанию количество дней можелирования системы */
    const DEFAULT_COUNT_WORK_DAY = 30;

    /** @var CarWorkDayFacade */
    private $carWorkDayFacade;
    /** @var CarManager */
    private $carManager;
    /** @var DateHelper */
    private $dateHelper;
    /** @var ReportFactory */
    private $reportFactory;

    /**
     * TaxiPoolFacade constructor.
     *
     * @param CarWorkDayFacade $carWorkDayFacade
     * @param CarManager       $carManager
     * @param DateHelper       $dateHelper
     * @param ReportFactory    $reportFactory
     */
    public function __construct(
        CarWorkDayFacade $carWorkDayFacade,
        CarManager $carManager,
        DateHelper $dateHelper,
        ReportFactory $reportFactory
    )
    {
        $this->carWorkDayFacade = $carWorkDayFacade;
        $this->carManager       = $carManager;
        $this->dateHelper       = $dateHelper;
        $this->reportFactory    = $reportFactory;
    }

    /**
     * @param TaxiPool $taxiPool
     *
     * @return \app\dto\ReportDto
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function exec(TaxiPool $taxiPool)
    {
        $currentDay = new DateTime();
        /** @var Report $report */
        $report     = \Yii::$container->get(Report::class);

        $simulationCountDays = $taxiPool->getSimulationCountDays() !== null ? $taxiPool->getSimulationCountDays() : static::DEFAULT_COUNT_WORK_DAY;
        foreach(range(1, $simulationCountDays) as $day) {
            $this->renewGenerator($taxiPool);

            while(true) {
                $car               = $taxiPool->getCarCollection()->getNextValue();
                $carIsNull         = $car === null;
                $reportCarIsBroken = $this->createReportCarIsBroken($car, $currentDay, $carIsNull);
                if ($reportCarIsBroken !== null) {
                    $report->addReport($reportCarIsBroken, $currentDay);
                    continue;
                }

                $driver       = $taxiPool->getDriverCollection()->getNextValue();
                $driverIsNull = $driver === null;
                if ($carIsNull && $driverIsNull) {
                    //выход из цикла так как авто и водители закончились
                    break;
                }

                $reportOneEntityIsNull = $this->createReportOneEntityIsNull($car, $driver);
                if ($reportOneEntityIsNull !== null) {
                    $report->addReport($reportOneEntityIsNull, $currentDay);
                    continue;
                }
                $report->addReport($this->carWorkDayFacade->exec($car, $driver, $currentDay),
                                   $currentDay
                );
            }
            $this->dateHelper->increaseDateNow($currentDay);
        }

        return $report->getReportDto();
    }

    /**
     * Обновить генераторы для коллекций машины и водители
     *
     * @param TaxiPool $taxiPool
     */
    private function renewGenerator(TaxiPool $taxiPool)
    {
        $taxiPool->getCarCollection()->renewGenerator();
        $taxiPool->getDriverCollection()->renewGenerator();
    }

    /**
     * Если авто сломан то создается соотвествующий дневной отчет
     *
     * @param Car|null $car
     * @param DateTime $currentDay
     * @param bool     $carIsNull
     *
     * @return ReportDaily|null
     */
    private function createReportCarIsBroken(Car $car = null, DateTime $currentDay, bool $carIsNull)
    {
        $report = null;
        if (!$carIsNull && $this->carManager->carIsBroken($car, $currentDay)) {
            $report = $this->reportFactory->createCarBroken($car);
        }

        return $report;
    }

    /**
     * Если свободное авто или водитель отсутствует, то создается соотвествующий дневной отчет
     *
     * @param Car|null    $car
     * @param Driver|null $driver
     *
     * @return ReportDaily|null
     */
    private function createReportOneEntityIsNull(Car $car = null, Driver $driver = null)
    {
        $report    = null;
        $carIsNull = $car === null;
        if ($carIsNull || $driver === null) {
            if ($carIsNull) {
                $report = $this->reportFactory->createDriverWithoutCar($driver);
            } else {
                $report = $this->reportFactory->createCarWithoutDriver($car);
            }
        }

        return $report;
    }
}