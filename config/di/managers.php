<?php

use yii\di\Instance;

$container->setDefinitions(
    [
        \app\managers\CarManager::class                         => [
            ['class' => \app\managers\CarManager::class],
            [
                Instance::of(\app\utils\DateHelper::class),
            ],
        ],
    ]
);