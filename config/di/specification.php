<?php

use yii\di\Instance;

$container->setDefinitions(
    [
        \app\specification\taxipool\TaxiPoolSpecification::class                         => [
            ['class' => \app\specification\taxipool\TaxiPoolSpecification::class],
            [
                Instance::of(\app\specification\taxipool\TaxiPoolCountPlacesMoreCarsSpecification::class),
            ],
        ],
        \app\specification\driver\DriverSpecification::class                         => [
            ['class' => \app\specification\driver\DriverSpecification::class],
            [
                Instance::of(\app\specification\driver\DriverValidTypeSpecification::class),
            ],
        ],
        \app\specification\car\CarSpecification::class                         => [
            ['class' => \app\specification\car\CarSpecification::class],
            [
                Instance::of(\app\specification\car\CarValidBrandSpecification::class),
                Instance::of(\app\specification\car\CarValidKmSpecification::class),
            ],
        ],
    ]
);