<?php

//переменные используются в подключаемых файлах
$container                 = Yii::$container;

require(__DIR__ . '/di/factories.php');
require(__DIR__ . '/di/utils.php');
require(__DIR__ . '/di/managers.php');
require(__DIR__ . '/di/specification.php');
