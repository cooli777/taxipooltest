<?php

namespace app\tests\unit\specification\driver;


use app\dto\DriverDto;
use app\exceptions\driver\DriverInvalidTypeException;
use app\specification\driver\DriverValidTypeSpecification;

class DriverValidTypeSpecificationTest extends \Codeception\Test\Unit
{
    /**
     * Проверка валидации типа водителя
     *
     * @dataProvider additionProvider
     *
     * @param DriverDto $driverDto
     * @param string    $exeptionExpected
     */
    public function testIsSatisfiedBy(DriverDto $driverDto, $exeptionExpected)
    {
        $driverValidTypeSpecification = new DriverValidTypeSpecification();
        if ($exeptionExpected) {
            $this->expectException($exeptionExpected);
        }
        $driverValidTypeSpecification->isSatisfiedBy($driverDto);
    }

    public function additionProvider()
    {
        return [
            [
                $this->createDriverDto('pro'),
                null,
            ],
            [
                $this->createDriverDto('pRo'),
                null,
            ],
            [
                $this->createDriverDto('PRo'),
                null,
            ],
            [
                $this->createDriverDto('default'),
                null,
            ],
            [
                $this->createDriverDto('default1'),
                DriverInvalidTypeException::class,
            ],
            [
                $this->createDriverDto(null),
                DriverInvalidTypeException::class,
            ],
            [
                $this->createDriverDto(100),
                DriverInvalidTypeException::class,
            ],
            [
                $this->createDriverDto([]),
                DriverInvalidTypeException::class,
            ],
        ];
    }

    /**
     * @param $type
     *
     * @return DriverDto
     */
    private function createDriverDto($type)
    {
        return DriverDto::loadFromArray(['type' => $type]);
    }
}