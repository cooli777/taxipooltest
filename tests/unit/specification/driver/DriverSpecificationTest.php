<?php
/**
 * Created by PhpStorm.
 * User: johnbrown
 * Date: 21.07.2018
 * Time: 20:11
 */

namespace app\tests\unit\specification\driver;


use app\dto\DriverDto;
use app\exceptions\driver\DriverInvalidDataException;
use app\exceptions\driver\DriverInvalidTypeException;
use app\specification\driver\DriverSpecification;
use app\specification\driver\DriverValidTypeSpecification;

class DriverSpecificationTest extends \Codeception\Test\Unit
{
    /**
     * Проверка валидации водителя
     *
     * @dataProvider additionProvider
     *
     * @param int    $countCars
     * @param string $exeptionExpected
     *
     * @throws \app\exceptions\taxipool\TaxiPoolPlacesLessException
     */
    public function testIsSatisfiedBy($exeption, $exeptionExpected)
    {
        $driverValidTypeSpecification = $this->getMockBuilder(DriverValidTypeSpecification::class)
            ->getMock();

        $driverValidTypeSpecificationWill = $driverValidTypeSpecification->expects($this->once())
            ->method('isSatisfiedBy');

        if ($exeption) {
            $driverValidTypeSpecificationWill->will($this->throwException($exeption));
        }

        $driverDto = new DriverDto();
        $driverValidTypeSpecification = new DriverSpecification($driverValidTypeSpecification);
        if ($exeptionExpected) {
            $this->expectException($exeptionExpected);
        }
        $driverValidTypeSpecification->isSatisfiedBy($driverDto);
    }

    public function additionProvider()
    {
        return [
            [
                new DriverInvalidTypeException(),
                DriverInvalidDataException::class,
            ],
            [
                null,
                null,
            ],
        ];
    }
}