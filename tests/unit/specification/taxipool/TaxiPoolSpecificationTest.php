<?php
/**
 * Created by PhpStorm.
 * User: johnbrown
 * Date: 21.07.2018
 * Time: 20:11
 */

namespace app\tests\unit\specification\taxipool;


use app\entity\car\CarCollection;
use app\entity\driver\DriverCollection;
use app\entity\TaxiPool;
use app\exceptions\taxipool\TaxiPoolInvalidDataException;
use app\exceptions\taxipool\TaxiPoolPlacesLessException;
use app\specification\taxipool\TaxiPoolCountPlacesMoreCarsSpecification;
use app\specification\taxipool\TaxiPoolSpecification;

class TaxiPoolSpecificationTest extends \Codeception\Test\Unit
{
    /**
     * Проверка валидации парковки
     *
     * @dataProvider additionProvider
     *
     * @param int    $countCars
     * @param string $exeptionExpected
     *
     * @throws \app\exceptions\taxipool\TaxiPoolPlacesLessException
     */
    public function testIsSatisfiedBy($exeption, $exeptionExpected)
    {
        $driverCollection = $this->getMockBuilder(DriverCollection::class)
            ->getMock();
        $carCollection    = $this->getMockBuilder(CarCollection::class)
            ->getMock();

        $taxiPoolCountPlacesMoreCarsSpecification = $this->getMockBuilder(TaxiPoolCountPlacesMoreCarsSpecification::class)
            ->getMock();


        $taxiPoolCountPlacesMoreCarsSpecificationWill = $taxiPoolCountPlacesMoreCarsSpecification->expects($this->once())
            ->method('isSatisfiedBy');

        if ($exeption) {
            $taxiPoolCountPlacesMoreCarsSpecificationWill->will($this->throwException($exeption));
        }

        $taxiPool = new TaxiPool($driverCollection, $carCollection);
        $taxiPoolCountPlacesMoreCarsSpecification = new TaxiPoolSpecification($taxiPoolCountPlacesMoreCarsSpecification);
        if ($exeptionExpected) {
            $this->expectException($exeptionExpected);
        }
        $taxiPoolCountPlacesMoreCarsSpecification->isSatisfiedBy($taxiPool);
    }

    public function additionProvider()
    {
        return [
            [
                new TaxiPoolPlacesLessException(),
                TaxiPoolInvalidDataException::class,
            ],
            [
                null,
                null,
            ],
        ];
    }
}