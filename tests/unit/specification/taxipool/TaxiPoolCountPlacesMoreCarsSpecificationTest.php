<?php
/**
 * Created by PhpStorm.
 * User: johnbrown
 * Date: 21.07.2018
 * Time: 20:11
 */

namespace app\tests\unit\specification\taxipool;


use app\entity\car\CarCollection;
use app\entity\driver\DriverCollection;
use app\entity\TaxiPool;
use app\exceptions\taxipool\TaxiPoolPlacesLessException;
use app\specification\taxipool\TaxiPoolCountPlacesMoreCarsSpecification;

class TaxiPoolCountPlacesMoreCarsSpecificationTest extends \Codeception\Test\Unit
{
    /**
     * Проверка валидации количество парковочных мест
     *
     * @dataProvider additionProvider
     *
     * @param int $countPlaces
     * @param int $countCars
     * @param string $exeptionExpected
     *
     * @throws \app\exceptions\taxipool\TaxiPoolPlacesLessException
     */
    public function testIsSatisfiedBy($countPlaces, $countCars ,$exeptionExpected)
    {
        $driverCollection = $this->getMockBuilder(DriverCollection::class)
            ->getMock();
        $carCollection = $this->getMockBuilder(CarCollection::class)
            ->getMock();

        $carCollection->expects($this->once())
            ->method('getCountCars')
            ->will($this->returnValue($countCars));

        $taxiPool = new TaxiPool($driverCollection, $carCollection);
        $taxiPool->setPlaces($countPlaces);
        $taxiPoolCountPlacesMoreCarsSpecification = new TaxiPoolCountPlacesMoreCarsSpecification();
        if($exeptionExpected){
            $this->expectException($exeptionExpected);
        }
        $taxiPoolCountPlacesMoreCarsSpecification->isSatisfiedBy($taxiPool);
    }

    public function additionProvider()
    {
        return [
            [
                3,
                3,
                null
            ],
            [
                3,
                5,
                TaxiPoolPlacesLessException::class
            ],
            [
                30,
                5,
                null
            ],
        ];
    }
}