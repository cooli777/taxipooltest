<?php

namespace app\tests\unit\specification\car;


use app\dto\CarDto;
use app\exceptions\car\CarInvalidKmException;
use app\specification\car\CarValidKmSpecification;

class CarValidKmSpecificationTest extends \Codeception\Test\Unit
{
    /**
     * Проверка валидации авто верности км
     *
     * @dataProvider additionProvider
     *
     * @param CarDto $carDto
     * @param string $exeptionExpected
     *
     * @throws CarInvalidKmException
     */
    public function testIsSatisfiedBy(CarDto $carDto, $exeptionExpected)
    {
        $driverValidTypeSpecification = new CarValidKmSpecification();
        if ($exeptionExpected) {
            $this->expectException($exeptionExpected);
        }
        $driverValidTypeSpecification->isSatisfiedBy($carDto);
    }

    public function additionProvider()
    {
        return [
            [
                $this->createCarDto(100),
                null,
            ],
            [
                $this->createCarDto('pRo'),
                CarInvalidKmException::class,
            ],
            [
                $this->createCarDto(null),
                CarInvalidKmException::class,
            ],
            [
                $this->createCarDto(100.05),
                CarInvalidKmException::class,
            ],
        ];
    }

    /**
     * @param $km
     *
     * @return CarDto
     */
    private function createCarDto($km)
    {
        return CarDto::loadFromArray(['km' => $km]);
    }
}