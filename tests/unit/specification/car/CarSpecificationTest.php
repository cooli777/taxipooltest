<?php
/**
 * Created by PhpStorm.
 * User: johnbrown
 * Date: 21.07.2018
 * Time: 20:11
 */

namespace app\tests\unit\specification\driver;


use app\dto\CarDto;
use app\exceptions\car\CarInvalidBrandException;
use app\exceptions\car\CarInvalidDataException;
use app\exceptions\car\CarInvalidKmException;
use app\specification\car\CarSpecification;
use app\specification\car\CarValidBrandSpecification;
use app\specification\car\CarValidKmSpecification;

class CarSpecificationTest extends \Codeception\Test\Unit
{
    /**
     * Проверка валидации авто ошибка км
     *
     * @dataProvider additionProviderKm
     *
     * @param $exceptionKm
     * @param $exceptionExpected
     *
     * @throws CarInvalidDataException
     */
    public function testIsSatisfiedByKm($exceptionKm, $exceptionExpected)
    {
        $carValidKmSpecification = $this->getMockBuilder(CarValidKmSpecification::class)
            ->getMock();

        $carValidKmSpecificationWill = $carValidKmSpecification->expects($this->any())
            ->method('isSatisfiedBy');

        if ($exceptionKm) {
            $carValidKmSpecificationWill->will($this->throwException($exceptionKm));
        }

        $carDto = new CarDto();
        $carValidKmSpecification = new CarSpecification($carValidKmSpecification);
        if ($exceptionExpected) {
            $this->expectException($exceptionExpected);
        }
        $carValidKmSpecification->isSatisfiedBy($carDto);
    }

    /**
     * Проверка валидации авто  ошибка бренд
     *
     * @dataProvider additionProviderBrand
     *
     * @param $exceptionBrand
     * @param $exceptionExpected
     *
     * @throws CarInvalidDataException
     */
    public function testIsSatisfiedByBrand($exceptionBrand, $exceptionExpected)
    {
        $carValidBrandSpecification = $this->getMockBuilder(CarValidBrandSpecification::class)
            ->getMock();

        $carValidBrandSpecificationWill = $carValidBrandSpecification->expects($this->any())
            ->method('isSatisfiedBy');

        if ($exceptionBrand) {
            $carValidBrandSpecificationWill->will($this->throwException($exceptionBrand));
        }

        $carDto = new CarDto();
        $carValidKmSpecification = new CarSpecification($carValidBrandSpecification);
        if ($exceptionExpected) {
            $this->expectException($exceptionExpected);
        }
        $carValidKmSpecification->isSatisfiedBy($carDto);
    }

    public function additionProviderKm()
    {
        return [
            [
                new CarInvalidKmException(),
                CarInvalidDataException::class,
            ],
            [
                null,
                null,
            ],
        ];
    }

    public function additionProviderBrand()
    {
        return [
            [
                new CarInvalidBrandException(),
                CarInvalidDataException::class,
            ],
            [
                null,
                null,
            ],
        ];
    }
}