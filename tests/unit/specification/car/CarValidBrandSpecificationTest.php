<?php

namespace app\tests\unit\specification\car;


use app\dto\CarDto;
use app\exceptions\car\CarInvalidBrandException;
use app\exceptions\car\CarInvalidKmException;
use app\specification\car\CarValidBrandSpecification;
use app\specification\car\CarValidKmSpecification;

class CarValidBrandSpecificationTest extends \Codeception\Test\Unit
{
    /**
     * Проверка валидации авто верности бренда
     *
     * @dataProvider additionProvider
     *
     * @param CarDto $carDto
     * @param string $exeptionExpected
     *
     * @throws CarInvalidKmException
     */
    public function testIsSatisfiedBy(CarDto $carDto, $exeptionExpected)
    {
        $driverValidTypeSpecification = new CarValidBrandSpecification();
        if ($exeptionExpected) {
            $this->expectException($exeptionExpected);
        }
        $driverValidTypeSpecification->isSatisfiedBy($carDto);
    }

    public function additionProvider()
    {
        return [
            [
                $this->createCarDto('hendai'),
                null,
            ],
            [
                $this->createCarDto('homba'),
                null,
            ],
            [
                $this->createCarDto('hoMba'),
                null,
            ],
            [
                $this->createCarDto('luda'),
                null,
            ],
            [
                $this->createCarDto('pRo'),
                CarInvalidBrandException::class,
            ],
            [
                $this->createCarDto(null),
                CarInvalidBrandException::class,
            ],
            [
                $this->createCarDto(100.05),
                CarInvalidBrandException::class,
            ],
            [
                $this->createCarDto(100),
                CarInvalidBrandException::class,
            ],
            [
                $this->createCarDto([]),
                CarInvalidBrandException::class,
            ],
        ];
    }

    /**
     * @param $brand
     *
     * @return CarDto
     */
    private function createCarDto($brand)
    {
        return CarDto::loadFromArray(['brand' => $brand]);
    }
}