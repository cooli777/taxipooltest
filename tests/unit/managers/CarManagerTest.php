<?php

namespace app\tests\unit\sort;


use app\entity\car\Car;
use app\entity\driver\Driver;
use app\factories\CarFactory;
use app\factories\DriverFactory;
use app\managers\CarManager;
use app\specification\car\CarSpecification;
use app\specification\driver\DriverSpecification;
use app\utils\DateHelper;

class CarManagerTest extends \Codeception\Test\Unit
{
    /**
     * Проверка расчет расхода топлива
     *
     * @dataProvider additionProviderFuelConsumption
     *
     * @param Driver[] $driversDtos
     * @param array    $exeptionExpected
     */
    public function testGetFuelConsumptionByDistance(Car $car, Driver $driver, $km, $expectedFuelConsumption)
    {
        $carManager = $this->createCarManager();
        $fuelConsumption = $carManager->getFuelConsumptionByDistance($car, $driver, $km);
        $this->assertEquals($expectedFuelConsumption, $fuelConsumption, 'Расхода топлива.');
    }

    public function additionProviderFuelConsumption()
    {
        return [
            [
                $this->createCar('luda', 100),
                $this->createDriver('pro'),
                100,
                8.0
            ],
            [
                $this->createCar('luda', 100),
                $this->createDriver('default'),
                100,
                10.0
            ],
            [
                $this->createCar('homba', 100),
                $this->createDriver('default'),
                100,
                7.0
            ],
            [
                $this->createCar('homba', 100),
                $this->createDriver('pro'),
                100,
                5.6
            ],
            [
                $this->createCar('hendai', 100),
                $this->createDriver('pro'),
                100,
                8.0
            ],
            [
                $this->createCar('hendai', 100),
                $this->createDriver('default'),
                100,
                10.0
            ],
        ];
    }

    /**
     * Проверка расчет коэффициента поломки
     *
     * @dataProvider additionProviderBreakdownRatio
     *
     * @param Driver[] $driversDtos
     * @param array    $exeptionExpected
     */
    public function testGetBreakdownRatio(Car $car, $expectedBreakdownRatio)
    {
        $carManager = $this->createCarManager();
        $breakdownRatio = $carManager->getBreakdownRatio($car);
        $this->assertEquals($expectedBreakdownRatio, $breakdownRatio, 'Коэффициент поломки.');
    }

    public function additionProviderBreakdownRatio()
    {
        return [
            [
                $this->createCar('homba', 10000),
                10.5,
            ],
            [
                $this->createCar('hendai', 10000),
                10.5,
            ],
            [
                $this->createCar('Luda', 10000),
                31.5,
            ],
            [
                $this->createCar('Luda', 1000000),
                100.0,
            ],
        ];
    }

    /**
     * @return CarManager
     */
    private function createCarManager()
    {
        $dateHelper = new DateHelper();
        return new CarManager($dateHelper);
    }


    /**
     * @param $type
     *
     * @return Driver
     */
    private function createDriver($type)
    {
        $driverSpecification = $this->getMockBuilder(DriverSpecification::class)
            ->getMock();

        $driverSpecification->expects($this->once())->method('isSatisfiedBy');

        $driverFactory = new DriverFactory($driverSpecification);

        return $driverFactory->createDriver([ 'type' => $type ]);
    }

    /**
     * @param $brand
     *
     * @param $km
     *
     * @return Car
     */
    private function createCar($brand, $km)
    {
        $carSpecification = $this->getMockBuilder(CarSpecification::class)
            ->getMock();

        $carSpecification->expects($this->once())->method('isSatisfiedBy');

        $driverFactory = new CarFactory($carSpecification);

        return $driverFactory->createCar([ 'brand' => $brand, 'km' =>$km ]);
    }
}