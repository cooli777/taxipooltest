<?php

namespace app\tests\unit\sort;


use app\entity\driver\Driver;
use app\factories\DriverFactory;
use app\sort\DriverSort;
use app\specification\driver\DriverSpecification;
use Generator;

class DriverSortTest extends \Codeception\Test\Unit
{
    /**
     * Проверка сортировки водителей
     *
     * @dataProvider additionProvider
     *
     * @param Driver[] $driversDtos
     * @param array    $exeptionExpected
     */
    public function testSortByType(array $driversDtos, $expectedArray)
    {
        $driverSort        = new DriverSort();
        $driversDtosSorted = $driverSort->sortByType($driversDtos);

        $this->assertEquals(count($expectedArray), count($driversDtosSorted), 'Число элементов в массиве.');

        $generatorExpectedArray = $this->getGenerator($expectedArray);
        foreach($driversDtosSorted as $driver) {
            $this->assertEquals($this->getNextValue($generatorExpectedArray), $driver->getType(), 'Сравнение типов водителей элементов.');
        }
    }

    public function additionProvider()
    {
        return [
            [
                $this->createDriversDtos([ 'pro', 'default', 'pro', 'default', 'default', 'pro', ]),
                [ 'pro', 'pro', 'pro', 'default', 'default', 'default', ],
            ],
            [
                $this->createDriversDtos([ 'default', 'default', 'default', 'default', 'pro', 'pro', ]),
                [ 'pro', 'pro', 'default', 'default', 'default', 'default', ],
            ],
            [
                $this->createDriversDtos([ 'pro', 'pro', 'default', 'default', 'default', 'default', ]),
                [ 'pro', 'pro', 'default', 'default', 'default', 'default', ],
            ],
        ];
    }

    private function createDriversDtos(array $typesDrivers)
    {
        if (count($typesDrivers) === 0 || $typesDrivers === null) {
            return $typesDrivers;
        }

        return array_map(function(string $typeDriver)
        {
            return $this->createDriver($typeDriver);
        }, $typesDrivers
        );
    }


    /**
     * @param $type
     *
     * @return Driver
     */
    private function createDriver($type)
    {
        $driverSpecification = $this->getMockBuilder(DriverSpecification::class)
            ->getMock();

        $driverSpecification->expects($this->once())->method('isSatisfiedBy');

        $driverFactory = new DriverFactory($driverSpecification);

        return $driverFactory->createDriver([ 'type' => $type ]);
    }

    /**
     * Получение генератора
     *
     * @param string[] $drivers
     *
     * @return Generator
     */
    private function getGenerator($drivers)
    {
        foreach($drivers as $driver) {
            yield $driver;
        }
    }

    /**
     * Получение следуещего типа водителя из генератора
     *
     * @param Generator $generator
     *
     * @return string
     */
    public function getNextValue(Generator $generator)
    {
        $value = $generator->current();
        $generator->next();

        return $value;
    }
}