<?php

namespace app\tests\unit\sort;


use app\entity\car\Car;
use app\entity\driver\Driver;
use app\factories\CarFactory;
use app\sort\CarSort;
use app\specification\car\CarSpecification;
use Generator;

class CarSortTest extends \Codeception\Test\Unit
{
    /**
     * Проверка сортировки массива авто
     *
     * @dataProvider additionProvider
     *
     * @param Car[] $carsDtos
     * @param array    $exeptionExpected
     */
    public function testSortByType(array $carsDtos, $expectedArray)
    {
        $carSort        = new CarSort();
        $carsDtosSorted = $carSort->sortByBrand($carsDtos);

        $this->assertEquals(count($expectedArray), count($carsDtosSorted), 'Число элементов в массиве.');

        $generatorExpectedArray = $this->getGenerator($expectedArray);
        foreach($carsDtosSorted as $car) {
            $this->assertEquals($this->getNextValue($generatorExpectedArray), $car->getBrand(), 'Сравнение бренда авто элементов.');
        }
    }

    public function additionProvider()
    {
        return [
            [
                $this->createCarsDtos([ 'homba', 'hendai', 'luda', 'homba', 'luda', 'hendai', ]),
                [ 'hendai', 'hendai', 'homba', 'homba', 'luda', 'luda', ],
            ],
            [
                $this->createCarsDtos([ 'luda', 'homba', 'hendai', 'luda', 'homba', 'luda', 'hendai', ]),
                [ 'hendai', 'hendai', 'homba', 'homba', 'luda', 'luda', 'luda', ],
            ],
        ];
    }

    private function createCarsDtos(array $carBrand)
    {
        if (count($carBrand) === 0 || $carBrand === null) {
            return $carBrand;
        }

        return array_map(function(string $typeDriver)
        {
            return $this->createCar($typeDriver);
        }, $carBrand
        );
    }


    /**
     * @param $type
     *
     * @return Driver
     */
    private function createCar($type)
    {
        $carSpecification = $this->getMockBuilder(CarSpecification::class)
            ->getMock();

        $carSpecification->expects($this->once())->method('isSatisfiedBy');

        $driverFactory = new CarFactory($carSpecification);

        return $driverFactory->createCar([ 'brand' => $type ]);
    }

    /**
     * Получение генератора
     *
     * @param string[] $cars
     *
     * @return Generator
     */
    private function getGenerator($cars)
    {
        foreach($cars as $car) {
            yield $car;
        }
    }

    /**
     * Получение следуещего типа водителя из генератора
     *
     * @param Generator $generator
     *
     * @return string
     */
    public function getNextValue(Generator $generator)
    {
        $value = $generator->current();
        $generator->next();

        return $value;
    }
}