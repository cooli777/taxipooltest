<?php

class DateHelperTest extends \Codeception\Test\Unit
{
    /**
     * Проверка вычисления разницы дней
     */
    public function testDifferenceCountDaysBetweenBrokenAndCurrantDate()
    {
        $dateHelper = new \app\utils\DateHelper();
        $brokenDate = new DateTime('2018-01-01');
        $currentDate = new DateTime('2018-01-04');
        $countDays = $dateHelper->differenceCountDaysBetweenBrokenAndCurrantDate($brokenDate, $currentDate);

        $this->assertEquals(3, $countDays, 'Проверка разницы дней');
    }

    /**
     * Проверка получения формата даты в формате год-месяц
     */
    public function testGetDateYearMonth()
    {
        $dateHelper = new \app\utils\DateHelper();
        $currentDate = new DateTime('2018-01-04');
        $dataFormated = $dateHelper->getDateYearMonth($currentDate);

        $this->assertEquals('2018-01', $dataFormated, 'получения формата даты в формате год-месяц');
    }

    /**
     * Проверка получения формата даты в формате год-месяц-день
     */
    public function testGetDateYmd()
    {
        $dateHelper = new \app\utils\DateHelper();
        $currentDate = new DateTime('2018-01-04');
        $dataFormated = $dateHelper->getDateYmd($currentDate);

        $this->assertEquals('2018-01-04', $dataFormated, 'получения формата даты в формате год-месяц-день');
    }

    /**
     * Проверка  увеличение даты на 1 день
     */
    public function testIncreaseDateNow()
    {
        $dateHelper = new \app\utils\DateHelper();
        $currentDate = new DateTime('2018-01-04');

        $dateHelper->increaseDateNow($currentDate);

        $format = 'Y-m-d';
        $this->assertEquals('2018-01-05', $currentDate->format($format), 'увеличение даты на 1 день');
    }
}
