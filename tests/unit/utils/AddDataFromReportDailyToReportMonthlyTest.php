<?php

use app\entity\car\HendaiCar;
use app\entity\car\HombaCar;
use app\entity\car\LudaCar;
use app\entity\driver\DefaultDriver;
use app\entity\driver\ProDriver;
use app\entity\report\ReportDaily;
use app\entity\report\ReportMonthly;
use app\utils\AddDataFromReportDailyToReportMonthly;

class AddDataFromReportDailyToReportMonthlyTest extends \Codeception\Test\Unit
{
    /**
     * Проверка добавление данных из дневного отчета в месячный
     *
     * @dataProvider additionProvider
     *
     * @param ReportMonthly $reportMonthly
     * @param ReportDaily[] $reportsDaily
     * @param ReportMonthly $reportMonthlyExpected
     */
    public function testAddDataToReport(ReportMonthly $reportMonthly, $reportsDaily, ReportMonthly $reportMonthlyExpected)
    {
        $addDataFromReportDailyToReportMonthly = new AddDataFromReportDailyToReportMonthly();
        foreach($reportsDaily as $reportDaily) {
            $addDataFromReportDailyToReportMonthly->addDataToReport($reportMonthly, $reportDaily);
        }

        $this->assertEquals($reportMonthlyExpected->countCasesCarWithoutDriver, $reportMonthly->countCasesCarWithoutDriver, 'Количество случаев авто без водителя');
        $this->assertEquals($reportMonthlyExpected->countCasesDriverWithoutCar, $reportMonthly->countCasesDriverWithoutCar, 'Количество случаев водитель без авто');
        $this->assertEquals($reportMonthlyExpected->countBrokenCars, $reportMonthly->countBrokenCars, 'Количество случаев поломок авто');
        $this->assertEquals($reportMonthlyExpected->countDaysBrokenCars, $reportMonthly->countDaysBrokenCars, 'Количество дней когда авто было сломано');
        $this->assertEquals($reportMonthlyExpected->countBrokenCarsLuda, $reportMonthly->countBrokenCarsLuda, 'Количество случаев поломок авто Luda');
        $this->assertEquals($reportMonthlyExpected->countBrokenCarsHomba, $reportMonthly->countBrokenCarsHomba, 'Количество случаев поломок авто Homba');
        $this->assertEquals($reportMonthlyExpected->countBrokenCarsHendai, $reportMonthly->countBrokenCarsHendai, 'Количество случаев поломок авто Hendai');
        $this->assertEquals($reportMonthlyExpected->countCarOnRaceProbabilityFailureMore50Less75, $reportMonthly->countCarOnRaceProbabilityFailureMore50Less75, 'Количество случаев когда авто выходи ло в рейс с возможностью поломки от 50 до 74.99');
        $this->assertEquals($reportMonthlyExpected->countCarOnRaceProbabilityFailureMore75Less100, $reportMonthly->countCarOnRaceProbabilityFailureMore75Less100, 'Количество случаев когда авто выходи ло в рейс с возможностью поломки от 75 до 99.99');
        $this->assertEquals($reportMonthlyExpected->countCarOnRaceProbabilityFailure100, $reportMonthly->countCarOnRaceProbabilityFailure100, 'Количество случаев когда авто выходи ло в рейс с возможностью поломки 100');
        $this->assertEquals($reportMonthlyExpected->countRouteCarsLuda, $reportMonthly->countRouteCarsLuda, 'Количество выходов в рейс авто Luda');
        $this->assertEquals($reportMonthlyExpected->countRouteCarsHomba, $reportMonthly->countRouteCarsHomba, 'Количество выходов в рейс авто Homba');
        $this->assertEquals($reportMonthlyExpected->countRouteCarsHendai, $reportMonthly->countRouteCarsHendai, 'Количество выходов в рейс авто Hendai');
        $this->assertEquals($reportMonthlyExpected->countRouteDriverDefault, $reportMonthly->countRouteDriverDefault, 'Количество выходов в рейс водителя тип Default');
        $this->assertEquals($reportMonthlyExpected->countRouteDriverPro, $reportMonthly->countRouteDriverPro, 'Количество выходов в рейс водителя тип Pro');
        $this->assertEquals($reportMonthlyExpected->carCountTraveledKm, $reportMonthly->carCountTraveledKm, 'Сколько всего км пройденно');
        $this->assertEquals($reportMonthlyExpected->countOrder, $reportMonthly->countOrder, 'Сколько всего заказов выполенно');
        $this->assertEquals($reportMonthlyExpected->fuelConsumption, $reportMonthly->fuelConsumption, 'Сколько всего топлива израсходаванно');
    }

    public function additionProvider()
    {
        return [
            [
                new ReportMonthly(),
                [
                    $this->createReportDaily(true, true, LudaCar::BRAND, false, false, 10.0, DefaultDriver::TYPE, 10, 110, 10, 5.0),
                ],
                $this->createReportMonthly(0,
                                           0,
                                           0,
                                           0,
                                           0,
                                           0,
                                           0,
                                           0,
                                           0,
                                           0,
                                           1,
                                           0,
                                           0,
                                           1,
                                           0,
                                           10,
                                           10,
                                           5.0
                ),

            ], [
                new ReportMonthly(),
                [
                    $this->createReportDaily(true, true, HombaCar::BRAND, false, false, 10.0, DefaultDriver::TYPE, 10, 110, 10, 5.0),
                    $this->createReportDaily(true, true, HombaCar::BRAND, false, false, 10.0, ProDriver::TYPE, 10, 110, 10, 5.0),
                    $this->createReportDaily(true, true, HendaiCar::BRAND, false, false, 10.0, DefaultDriver::TYPE, 10, 110, 10, 5.0),
                    $this->createReportDaily(true, true, HendaiCar::BRAND, false, false, 60.0, DefaultDriver::TYPE, 10, 110, 10, 5.0),
                    $this->createReportDaily(true, false, HendaiCar::BRAND, false, false, 60.0, null, 0, 110, 0, 0.0),
                    $this->createReportDaily(false, true, null, false, false, 0.0, ProDriver::TYPE, 0, 0, 0, 0.0),
                    $this->createReportDaily(true, true, HendaiCar::BRAND, true, true, 80.0, DefaultDriver::TYPE, 0, 110, 0, 0.0),
                    $this->createReportDaily(true, false, HendaiCar::BRAND, true, false, 80.0, null, 0, 110, 0, 0.0),
                    $this->createReportDaily(true, true, LudaCar::BRAND, true, true, 100.0, DefaultDriver::TYPE, 0, 110, 0, 0.0),
                    $this->createReportDaily(true, true, HombaCar::BRAND, true, true, 100.0, DefaultDriver::TYPE, 0, 110, 0, 0.0),
                    $this->createReportDaily(true, false, HombaCar::BRAND, true, false, 100.0, null, 0, 110, 0, 0.0),
                ],
                $this->createReportMonthly(1,
                                           1,
                                           3,
                                           5,
                                           1,
                                           1,
                                           1,
                                           1,
                                           1,
                                           2,
                                           1,
                                           3,
                                           3,
                                           6,
                                           1,
                                           40,
                                           40,
                                           20.0
                ),

            ],
        ];
    }

    private function createReportMonthly(
        $countCasesCarWithoutDriver,
        $countCasesDriverWithoutCar,
        $countBrokenCars,
        $countDaysBrokenCars,
        $countBrokenCarsLuda,
        $countBrokenCarsHomba,
        $countBrokenCarsHendai,
        $countCarOnRaceProbabilityFailureMore50Less75,
        $countCarOnRaceProbabilityFailureMore75Less100,
        $countCarOnRaceProbabilityFailure100,
        $countRouteCarsLuda,
        $countRouteCarsHomba,
        $countRouteCarsHendai,
        $countRouteDriverDefault,
        $countRouteDriverPro,
        $carCountTraveledKm,
        $countOrder,
        $fuelConsumption
    )
    {
        $reportMonthly = new ReportMonthly();

        $reportMonthly->countCasesCarWithoutDriver                    = $countCasesCarWithoutDriver;
        $reportMonthly->countCasesDriverWithoutCar                    = $countCasesDriverWithoutCar;
        $reportMonthly->countBrokenCars                               = $countBrokenCars;
        $reportMonthly->countDaysBrokenCars                           = $countDaysBrokenCars;
        $reportMonthly->countBrokenCarsLuda                           = $countBrokenCarsLuda;
        $reportMonthly->countBrokenCarsHomba                          = $countBrokenCarsHomba;
        $reportMonthly->countBrokenCarsHendai                         = $countBrokenCarsHendai;
        $reportMonthly->countCarOnRaceProbabilityFailureMore50Less75  = $countCarOnRaceProbabilityFailureMore50Less75;
        $reportMonthly->countCarOnRaceProbabilityFailureMore75Less100 = $countCarOnRaceProbabilityFailureMore75Less100;
        $reportMonthly->countCarOnRaceProbabilityFailure100           = $countCarOnRaceProbabilityFailure100;
        $reportMonthly->countRouteCarsLuda                            = $countRouteCarsLuda;
        $reportMonthly->countRouteCarsHomba                           = $countRouteCarsHomba;
        $reportMonthly->countRouteCarsHendai                          = $countRouteCarsHendai;
        $reportMonthly->countRouteDriverDefault                       = $countRouteDriverDefault;
        $reportMonthly->countRouteDriverPro                           = $countRouteDriverPro;
        $reportMonthly->carCountTraveledKm                            = $carCountTraveledKm;
        $reportMonthly->countOrder                                    = $countOrder;
        $reportMonthly->fuelConsumption                               = $fuelConsumption;

        return $reportMonthly;
    }

    private function createReportDaily(
        $haveCar,
        $haveDriver,
        $carBrand,
        $carIsBroken,
        $carIsBrokenToday,
        $probabilityFailure,
        $typeDriver,
        $carCountTraveledKmToday,
        $carCountTraveledKmAll,
        $countOrder,
        $fuelConsumption
    )
    {
        $reportDaily                          = new ReportDaily();
        $reportDaily->haveCar                 = $haveCar;
        $reportDaily->haveDriver              = $haveDriver;
        $reportDaily->carBrand                = $carBrand;
        $reportDaily->carIsBroken             = $carIsBroken;
        $reportDaily->carIsBrokenToday        = $carIsBrokenToday;
        $reportDaily->probabilityFailure      = $probabilityFailure;
        $reportDaily->typeDriver              = $typeDriver;
        $reportDaily->carCountTraveledKmToday = $carCountTraveledKmToday;
        $reportDaily->carCountTraveledKmAll   = $carCountTraveledKmAll;
        $reportDaily->countOrder              = $countOrder;
        $reportDaily->fuelConsumption         = $fuelConsumption;

        return $reportDaily;
    }
}
