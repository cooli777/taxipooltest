<?php

namespace app\utils;


use app\entity\car\HendaiCar;
use app\entity\car\HombaCar;
use app\entity\car\LudaCar;
use app\entity\driver\DefaultDriver;
use app\entity\driver\ProDriver;
use app\entity\report\ReportDaily;
use app\entity\report\ReportMonthly;

class AddDataFromReportDailyToReportMonthly
{
    /**
     * Добавление данных из дневного отчета в месячный
     *
     * @param ReportMonthly $reportMonthly
     * @param ReportDaily   $reportDaily
     */
    public function addDataToReport(ReportMonthly $reportMonthly, ReportDaily $reportDaily)
    {
        if($reportDaily->haveCar && !$reportDaily->haveDriver && $this->carBrokenInRoute($reportDaily)){
            $reportMonthly->countCasesCarWithoutDriver++;
        }
        if(!$reportDaily->haveCar && $reportDaily->haveDriver) {
            $reportMonthly->countCasesDriverWithoutCar++;
        }

        $this->addDataIfCarIsBroken($reportMonthly, $reportDaily);

        if($reportDaily->carIsBroken) {
            $reportMonthly->countDaysBrokenCars++;
        }

        $this->addDataIfProbabilityFailureMore50($reportMonthly, $reportDaily);
        $this->addDataCountRouteCarBrand($reportMonthly, $reportDaily);
        $this->addDataCountRouteDriverType($reportMonthly, $reportDaily);

        $reportMonthly->carCountTraveledKm += $reportDaily->carCountTraveledKmToday;
        $reportMonthly->countOrder += $reportDaily->countOrder;
        $reportMonthly->fuelConsumption += $reportDaily->fuelConsumption;
    }

    /**
     * Если авто сломан, то инкрементировать количество сломанных авто, также инкрементировать какой бренд авто сломан
     *
     * @param ReportMonthly $reportMonthly
     * @param ReportDaily   $reportDaily
     */
    private function addDataIfCarIsBroken(ReportMonthly $reportMonthly, ReportDaily $reportDaily)
    {
        if($reportDaily->carIsBrokenToday) {
            $reportMonthly->countBrokenCars++;
            //todo проверить какое авто сломалось
            switch($reportDaily->carBrand){
                case LudaCar::BRAND:
                    $reportMonthly->countBrokenCarsLuda++;
                    break;
                case HombaCar::BRAND:
                    $reportMonthly->countBrokenCarsHomba++;
                    break;
                case HendaiCar::BRAND:
                    $reportMonthly->countBrokenCarsHendai++;
                    break;
            }
        }
    }

    /**
     * Если процент поломки выше 50% тогда увеличить об этом запись
     *
     * @param ReportMonthly $reportMonthly
     * @param ReportDaily   $reportDaily
     */
    private function addDataIfProbabilityFailureMore50(ReportMonthly $reportMonthly, ReportDaily $reportDaily)
    {
        if(!$this->checkCarWork($reportDaily)){
            return;
        }

        if($reportDaily->probabilityFailure >= 50 &&  $reportDaily->probabilityFailure < 75){
            $reportMonthly->countCarOnRaceProbabilityFailureMore50Less75++;
        }elseif($reportDaily->probabilityFailure >= 75 &&  $reportDaily->probabilityFailure < 100){
            $reportMonthly->countCarOnRaceProbabilityFailureMore75Less100++;
        }elseif($reportDaily->probabilityFailure == 100){
            $reportMonthly->countCarOnRaceProbabilityFailure100++;
        }
    }

    /**
     * Количество рабочих дней для бренда авто
     *
     * @param ReportMonthly $reportMonthly
     * @param ReportDaily   $reportDaily
     */
    private function addDataCountRouteCarBrand(ReportMonthly $reportMonthly, ReportDaily $reportDaily)
    {
        if(!$this->checkCarWork($reportDaily)){
            return;
        }

        switch($reportDaily->carBrand){
            case LudaCar::BRAND:
                $reportMonthly->countRouteCarsLuda++;
                break;
            case HombaCar::BRAND:
                $reportMonthly->countRouteCarsHomba++;
                break;
            case HendaiCar::BRAND:
                $reportMonthly->countRouteCarsHendai++;
                break;
        }
    }

    /**
     * Количество рабочих дней для типа водителя
     *
     * @param ReportMonthly $reportMonthly
     * @param ReportDaily   $reportDaily
     */
    private function addDataCountRouteDriverType(ReportMonthly $reportMonthly, ReportDaily $reportDaily)
    {
        if(!$this->checkDriverWork($reportDaily)){
            return;
        }

        switch($reportDaily->typeDriver){
            case DefaultDriver::TYPE:
                $reportMonthly->countRouteDriverDefault++;
                break;
            case ProDriver::TYPE:
                $reportMonthly->countRouteDriverPro++;
                break;
        }
    }

    /**
     * Может ли авто работать
     *
     * @param ReportDaily $reportDaily
     *
     * @return bool
     */
    private function checkCarWork(ReportDaily $reportDaily){
        return $reportDaily->haveDriver && $this->carBrokenInRoute($reportDaily);
    }

    /**
     * Может ли водитель работать
     *
     * @param ReportDaily $reportDaily
     *
     * @return bool
     */
    private function checkDriverWork(ReportDaily $reportDaily){
        return $reportDaily->haveCar && $this->carBrokenInRoute($reportDaily);
    }

    /**
     * Проверка на то что авто было в рабочем состоянии на начало маршрута
     *
     * @param ReportDaily $reportDaily
     *
     * @return bool
     */
    private function carBrokenInRoute(ReportDaily $reportDaily)
    {
        return (!$reportDaily->carIsBroken || $reportDaily->carIsBrokenToday);
    }
}