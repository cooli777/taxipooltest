<?php

namespace app\utils;


use PhpOffice\PhpWord\PhpWord;

class WordHelper
{

    /**
     * @param $renderHtml
     *
     * @return string
     */
    public function generateReport($renderHtml): string
    {
        $phpWord = new PhpWord();
        $styleTable    = [ 'borderSize' => 6, 'borderColor' => '006699', 'cellMargin' => 80 ];
        $styleFirstRow = [ 'borderBottomSize' => 18, 'borderBottomColor' => '0000FF', 'bgColor' => '66BBFF' ];


        $sectionStyleParams = [
            'orientation' => 'landscape',
            'marginTop' => \PhpOffice\PhpWord\Shared\Converter::pixelToTwip(10),
            'marginLeft' => 600,
            'marginRight' => 600,
            'colsNum' => 1,
            'pageNumberingStart' => 1,
            'borderBottomSize'=>100,
            'borderBottomColor'=>'000000'
        ];
        $phpWord->addTableStyle('FancyTable', $styleTable, $styleFirstRow);
        $section = $phpWord->addSection($sectionStyleParams);
        $sectionStyle = $section->getStyle();
        $sectionStyle->setOrientation($sectionStyle::ORIENTATION_LANDSCAPE);

        \PhpOffice\PhpWord\Shared\Html::addHtml($section, $renderHtml);

        $tempFile = tempnam(sys_get_temp_dir(), 'PHPWord_report_taxi_pool');
        $phpWord->save($tempFile);
        return $tempFile;
    }
}