<?php
/**
 * Created by PhpStorm.
 * User: johnbrown
 * Date: 19.07.2018
 * Time: 13:56
 */

namespace app\utils;


use DateTime;

class DateHelper
{
    const FORMAT_Y_M_D = 'Y-m-d';
    const FORMAT_Y_M   = 'Y-m';
    const ADD_TIME     = '+1 day';

    /**
     * Возвразает текущую дату
     *
     * @return DateTime
     */
    public function getDateNow()
    {
        return new DateTime();
    }

    /**
     * Увеличчивает текущую дату на один день
     *
     * @param DateTime $currentDate
     */
    public function increaseDateNow(DateTime $currentDate)
    {
        $currentDate->modify(static::ADD_TIME);
    }

    /**
     * Получить дату в формате год-месяц-день
     *
     * @param DateTime $currentDate
     *
     * @return string
     */
    public function getDateYmd(DateTime $currentDate)
    {
        return $currentDate->format(static::FORMAT_Y_M_D);
    }

    /**
     * Получить дату в формате год-месяц
     *
     * @param DateTime $currentDate
     *
     * @return string
     */
    public function getDateYearMonth(DateTime $currentDate)
    {
        return $currentDate->format(static::FORMAT_Y_M);
    }

    /**
     * Получить разницу дней
     *
     * @param DateTime $brokenDate
     * @param DateTime $currentDate
     *
     * @return int|false
     */
    public function differenceCountDaysBetweenBrokenAndCurrantDate(DateTime $brokenDate, DateTime $currentDate)
    {
        $interval = $brokenDate->diff($currentDate);

        return $interval->days;
    }
}