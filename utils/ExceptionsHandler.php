<?php

namespace app\utils;


use app\exceptions\DisplayWebException;
use yii\web\Response;

/**
 * Class ExceptionsHandler
 */
class ExceptionsHandler
{

    /**
     * Метод отлавливает \Exeption и обрабатывает их
     *
     * @param          $id
     * @param          $params
     * @param callable $function
     *
     * @return mixed
     * @throws \yii\base\ExitException
     */
    public function execute($id, $params, callable $function)
    {
        try {
            return $function($id, $params);

        } catch (\Exception $e) {
            $code = $e->getCode();

            switch (true) {
                case $e instanceof DisplayWebException:
                    $response         = \Yii::$app->response;
                    $response->format = Response::FORMAT_JSON;
                    $response->data   = [
                        'status' => 'error',
                        'error'  => [
                            'code'       => $e->getCode(),
                            'message'    => $e->getMessage(),
                            'properties' => $e->getProperties(),
                        ],
                    ];

                    \Yii::$app->end($response->statusCode, $response);

                    return;
                    break;
                default:
                    $this->logException($e);

                    $message = 'Internal server error';
            }

            $this->sendErrorResponse($code, $message);
        }

        return null;
    }

    /**
     * @param \Exception $e
     */
    protected function logException(\Exception $e)
    {
        \Yii::error($e);
    }

    /**
     * Sends error response
     *
     * @param int    $code
     * @param string $message
     */
    protected function sendErrorResponse($code, $message)
    {
        $response         = \Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->setStatusCode($code, $message);

        \Yii::$app->end(0, $response);
    }
}