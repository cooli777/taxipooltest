<?php

namespace app\exceptions\car;


use app\exceptions\DisplayWebException;
use Exception;

class CarInvalidBrandException extends \Exception implements DisplayWebException
{
    protected $code = 1010;
    const TEMPLATE_MESSAGE = 'Такой "%s" бренд не допустим';

    public function __construct($type = '', $code = 0, Exception $previous = null)
    {
        $this->message = sprintf(self::TEMPLATE_MESSAGE, $type);
        parent::__construct($this->message, $this->code, $previous);
    }

    /**
     * @return array
     */
    public function getProperties()
    {
        return [ 'brand' => $this->message ];
    }
}