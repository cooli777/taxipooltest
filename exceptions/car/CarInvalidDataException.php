<?php

namespace app\exceptions\car;


use app\exceptions\DisplayWebException;
use Exception;

class CarInvalidDataException extends Exception implements DisplayWebException
{
    protected $code = 1009;
    protected $message = 'Не валидные данные `Car`';
    protected $properties;

    public function __construct($errors = [], $code = 0, Exception $previous = null)
    {
        $this->properties = $errors;
        parent::__construct($this->message, $this->code, $previous);
    }

    /**
     * @return array
     */
    public function getProperties()
    {
        return $this->properties;
    }
}