<?php

namespace app\exceptions\car;


use app\exceptions\DisplayWebException;
use Exception;

class CarInvalidKmException extends \Exception implements DisplayWebException
{
    protected $code = 1011;
    const TEMPLATE_MESSAGE = 'Свойство `km` не яляется целым числом, вы передали: "%s".';

    public function __construct($type = '', $code = 0, Exception $previous = null)
    {
        $this->message = sprintf(self::TEMPLATE_MESSAGE, $type);
        parent::__construct($this->message, $this->code, $previous);
    }

    /**
     * @return array
     */
    public function getProperties()
    {
        return [ 'km' => $this->message ];
    }
}