<?php

namespace app\exceptions\taxipool;


use app\exceptions\DisplayWebException;
use Exception;

class TaxiPoolInvalidDataException extends Exception implements DisplayWebException
{
    protected $code = 1209;
    protected $message = 'Не валидные данные `TaxiPool`';
    protected $properties;

    public function __construct($errors = [], $code = 0, Exception $previous = null)
    {
        $this->properties = $errors;
        parent::__construct($this->message, $this->code, $previous);
    }

    /**
     * @return array
     */
    public function getProperties()
    {
        return $this->properties;
    }
}