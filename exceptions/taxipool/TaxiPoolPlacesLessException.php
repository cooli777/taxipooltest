<?php

namespace app\exceptions\taxipool;


use app\exceptions\DisplayWebException;
use Exception;

class TaxiPoolPlacesLessException extends Exception implements DisplayWebException
{
    protected $code = 1210;
    const TEMPLATE_MESSAGE = 'Количество `places` слишком маленькое, количество `cars` = "%s".';

    public function __construct($countCars = 0, $code = 0, Exception $previous = null)
    {
        $this->message = sprintf(self::TEMPLATE_MESSAGE, $countCars);
        parent::__construct($this->message, $this->code, $previous);
    }

    /**
     * @return array
     */
    public function getProperties()
    {
        return ['places'=>$this->message];
    }
}