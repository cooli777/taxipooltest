<?php

namespace app\exceptions;

/**
 * interface DisplayWebException
 */
interface DisplayWebException {
    /**
     * @return array
     */
    public function getProperties();
}