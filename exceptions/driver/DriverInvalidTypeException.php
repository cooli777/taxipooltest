<?php

namespace app\exceptions\driver;


use app\exceptions\DisplayWebException;
use Exception;

class DriverInvalidTypeException extends \Exception implements DisplayWebException
{
    protected $code = 1110;
    const TEMPLATE_MESSAGE = 'Такой "%s" тип водителя не допустим.';

    public function __construct($type = '', $code = 0, Exception $previous = null)
    {
        $this->message = sprintf(self::TEMPLATE_MESSAGE, $type);
        parent::__construct($this->message, $this->code, $previous);
    }

    /**
     * @return array
     */
    public function getProperties()
    {
        return [ 'type' => $this->message ];
    }
}