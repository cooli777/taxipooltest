<?php
/**
 * Created by PhpStorm.
 * User: johnbrown
 * Date: 20.07.2018
 * Time: 14:40
 */

namespace app\exceptions\driver;


use app\exceptions\DisplayWebException;
use Exception;

class DriverInvalidDataException extends Exception implements DisplayWebException
{
    protected $code = 1109;
    protected $message = 'Не валидные данные `Driver`';
    protected $properties;

    public function __construct($errors = [], $code = 0, Exception $previous = null)
    {
        $this->properties = $errors;
        parent::__construct($this->message, $this->code, $previous);
    }

    /**
     * @return array
     */
    public function getProperties()
    {
        return $this->properties;
    }
}