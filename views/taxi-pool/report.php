<?php


/* @var \app\dto\ReportDto $reportDto */

?>

<p style="text-indent: 40px;">
    Отчет за весь период
</p>

<table style="border: 3px #000000 double;  width: 100%" border="1" cellpadding="0" cellspacing="0">
    <thead>
    <tr style="text-align: center; font-weight: bold; ">
        <td style="border: 3px double;"><b>Количество случаев когда авто не нашлось водителя</b></td>
        <td style="border: 3px double;"><b>Количество случаев когда водителю не нашлось авто</b></td>
        <td style="border: 3px double;"><b>Количество случаев поломки всех авто</b></td>
        <td style="border: 3px double;"><b>Количество дней когда авто был сломан авто</b></td>
        <td style="border: 3px double;"><b>Количество поломок авто бренд "Luda"</b></td>
        <td style="border: 3px double;"><b>Количество поломок авто бренд "Homba"</b></td>
        <td style="border: 3px double;"><b>Количество поломок авто бренд "Hendai"</b></td>
        <td style="border: 3px double;"><b>Количество случаев когда авто выходил на рейс с вероятностью поломки от 50% до
                74.99%</b></td>
        <td style="border: 3px double;"><b>Количество случаев когда авто выходил на рейс с вероятностью поломки от 75% до
                99.99%</b></td>
        <td style="border: 3px double;"><b>Количество случаев когда авто выходил на рейс с вероятностью поломки 100%</b>
        </td>
        <td style="border: 3px double;"><b>Количество рейсов для бренда "Luda"</b></td>
        <td style="border: 3px double;"><b>Количество рейсов для бренда "Homba"</b></td>
        <td style="border: 3px double;"><b>Количество рейсов для бренда "Hendai"</b></td>
        <td style="border: 3px double;"><b>Количество рейсов совершенных тип водителя "Default"</b></td>
        <td style="border: 3px double;"><b>Количество рейсов совершенных тип водителя "Pro"</b></td>
        <td style="border: 3px double;"><b>Общий пробег км.</b></td>
        <td style="border: 3px double;"><b>Общее число выполненных заказов</b></td>
        <td style="border: 3px double;"><b>Общий расход толива л.</b></td>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td style="border: 3px double;"><?= $reportDto->reportAll->countCasesCarWithoutDriver ?></td>
        <td style="border: 3px double;"><?= $reportDto->reportAll->countCasesDriverWithoutCar ?></td>
        <td style="border: 3px double;"><?= $reportDto->reportAll->countBrokenCars ?></td>
        <td style="border: 3px double;"><?= $reportDto->reportAll->countDaysBrokenCars ?></td>
        <td style="border: 3px double;"><?= $reportDto->reportAll->countBrokenCarsLuda ?></td>
        <td style="border: 3px double;"><?= $reportDto->reportAll->countBrokenCarsHomba ?></td>
        <td style="border: 3px double;"><?= $reportDto->reportAll->countBrokenCarsHendai ?></td>
        <td style="border: 3px double;"><?= $reportDto->reportAll->countCarOnRaceProbabilityFailureMore50Less75 ?></td>
        <td style="border: 3px double;"><?= $reportDto->reportAll->countCarOnRaceProbabilityFailureMore75Less100 ?></td>
        <td style="border: 3px double;"><?= $reportDto->reportAll->countCarOnRaceProbabilityFailure100 ?></td>
        <td style="border: 3px double;"><?= $reportDto->reportAll->countRouteCarsLuda ?></td>
        <td style="border: 3px double;"><?= $reportDto->reportAll->countRouteCarsHomba ?></td>
        <td style="border: 3px double;"><?= $reportDto->reportAll->countRouteCarsHendai ?></td>
        <td style="border: 3px double;"><?= $reportDto->reportAll->countRouteDriverDefault ?></td>
        <td style="border: 3px double;"><?= $reportDto->reportAll->countRouteDriverPro ?></td>
        <td style="border: 3px double;"><?= $reportDto->reportAll->carCountTraveledKm ?></td>
        <td style="border: 3px double;"><?= $reportDto->reportAll->countOrder ?></td>
        <td style="border: 3px double;"><?= $reportDto->reportAll->fuelConsumption ?></td>
    </tr>
    </tbody>
</table>

<p style="text-indent: 40px;">
    Отчеты по месяцам
</p>
<table style="border: 3px #000000 double;" border="1" cellpadding="0" cellspacing="0">
    <thead>
    <tr style="text-align: center; font-weight: bold; ">
        <td style="border: 3px double;"><b>Дата</b></td>
        <td style="border: 3px double;"><b>Количество случаев когда авто не нашлось водителя</b></td>
        <td style="border: 3px double;"><b>Количество случаев когда водителю не нашлось авто</b></td>
        <td style="border: 3px double;"><b>Количество случаев поломки всех авто</b></td>
        <td style="border: 3px double;"><b>Количество дней когда авто был сломан авто</b></td>
        <td style="border: 3px double;"><b>Количество поломок авто бренд "Luda"</b></td>
        <td style="border: 3px double;"><b>Количество поломок авто бренд "Homba"</b></td>
        <td style="border: 3px double;"><b>Количество поломок авто бренд "Hendai"</b></td>
        <td style="border: 3px double;"><b>Количество случаев когда авто выходил на рейс с вероятностью поломки от 50% до
                74.99%</b></td>
        <td style="border: 3px double;"><b>Количество случаев когда авто выходил на рейс с вероятностью поломки от 75% до
                99.99%</b></td>
        <td style="border: 3px double;"><b>Количество случаев когда авто выходил на рейс с вероятностью поломки 100%</b>
        </td>
        <td style="border: 3px double;"><b>Количество рейсов для бренда "Luda"</b></td>
        <td style="border: 3px double;"><b>Количество рейсов для бренда "Homba"</b></td>
        <td style="border: 3px double;"><b>Количество рейсов для бренда "Hendai"</b></td>
        <td style="border: 3px double;"><b>Количество рейсов совершенных тип водителя "Default"</b></td>
        <td style="border: 3px double;"><b>Количество рейсов совершенных тип водителя "Pro"</b></td>
        <td style="border: 3px double;"><b>Общий пробег км.</b></td>
        <td style="border: 3px double;"><b>Общее число выполненных заказов</b></td>
        <td style="border: 3px double;"><b>Общий расход толива л.</b></td>
    </tr>
    </thead>
    <tbody>
    <?php foreach($reportDto->reportsMonthly as $month => $reportMonthly) : ?>
        <tr>
            <td style="border: 3px double;"><?= $month ?></td>
            <td style="border: 3px double;"><?= $reportMonthly->countCasesCarWithoutDriver ?></td>
            <td style="border: 3px double;"><?= $reportMonthly->countCasesDriverWithoutCar ?></td>
            <td style="border: 3px double;"><?= $reportMonthly->countBrokenCars ?></td>
            <td style="border: 3px double;"><?= $reportMonthly->countDaysBrokenCars ?></td>
            <td style="border: 3px double;"><?= $reportMonthly->countBrokenCarsLuda ?></td>
            <td style="border: 3px double;"><?= $reportMonthly->countBrokenCarsHomba ?></td>
            <td style="border: 3px double;"><?= $reportMonthly->countBrokenCarsHendai ?></td>
            <td style="border: 3px double;"><?= $reportMonthly->countCarOnRaceProbabilityFailureMore50Less75 ?></td>
            <td style="border: 3px double;"><?= $reportMonthly->countCarOnRaceProbabilityFailureMore75Less100 ?></td>
            <td style="border: 3px double;"><?= $reportMonthly->countCarOnRaceProbabilityFailure100 ?></td>
            <td style="border: 3px double;"><?= $reportMonthly->countRouteCarsLuda ?></td>
            <td style="border: 3px double;"><?= $reportMonthly->countRouteCarsHomba ?></td>
            <td style="border: 3px double;"><?= $reportMonthly->countRouteCarsHendai ?></td>
            <td style="border: 3px double;"><?= $reportMonthly->countRouteDriverDefault ?></td>
            <td style="border: 3px double;"><?= $reportMonthly->countRouteDriverPro ?></td>
            <td style="border: 3px double;"><?= $reportMonthly->carCountTraveledKm ?></td>
            <td style="border: 3px double;"><?= $reportMonthly->countOrder ?></td>
            <td style="border: 3px double;"><?= $reportMonthly->fuelConsumption ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<p style="text-indent: 40px;">
    Побробный отчет
</p>
<table style="border: 3px #000000 double;" border="1" cellpadding="0" cellspacing="0">
    <thead>
    <tr style="text-align: center; font-weight: bold; ">
        <td style="border: 3px double;"><b>Был авто</b></td>
        <td style="border: 3px double;"><b>Был водитель</b></td>
        <td style="border: 3px double;"><b>Бренд авто</b></td>
        <td style="border: 3px double;"><b>Авто сломан сегодня</b></td>
        <td style="border: 3px double;"><b>Авто сломан</b></td>
        <td style="border: 3px double;"><b>Вероятность поломки</b></td>
        <td style="border: 3px double;"><b>Тип водителя</b></td>
        <td style="border: 3px double;"><b>Пробег авто за сегодня км</b></td>
        <td style="border: 3px double;"><b>Пробег авто всего км</b></td>
        <td style="border: 3px double;"><b>Выполненно заказов</b></td>
        <td style="border: 3px double;"><b>Расход толива л.</b></td>
    </tr>
    </thead>
    <tbody>
    <?php foreach($reportDto->reportsDaily as $day => $reportsForDay) : ?>
        <tr>
            <td style="border: 3px double;" colspan="12">за день: <?= $day ?></td>
        </tr>
        <?php foreach($reportsForDay as $reportDaily) : ?>
            <tr>
                <td style="border: 3px double;"><?= $reportDaily->haveCar ? "Да" : "Нет" ?></td>
                <td style="border: 3px double;"><?= $reportDaily->haveDriver ? "Да" : "Нет" ?></td>
                <td style="border: 3px double;"><?= $reportDaily->carBrand ?></td>
                <td style="border: 3px double;"><?= $reportDaily->carIsBrokenToday ? "Да" : "Нет" ?></td>
                <td style="border: 3px double;"><?= $reportDaily->carIsBroken ? "Да" : "Нет" ?></td>
                <td style="border: 3px double;"><?= $reportDaily->probabilityFailure ?></td>
                <td style="border: 3px double;"><?= $reportDaily->typeDriver ?></td>
                <td style="border: 3px double;"><?= $reportDaily->carCountTraveledKmToday ?></td>
                <td style="border: 3px double;"><?= $reportDaily->carCountTraveledKmAll ?></td>
                <td style="border: 3px double;"><?= $reportDaily->countOrder ?></td>
                <td style="border: 3px double;"><?= $reportDaily->fuelConsumption ?></td>
            </tr>

        <?php endforeach; ?>
    <?php endforeach; ?>
    </tbody>
</table>
