<?php

namespace app\dto;

/**
 * Class CarDto
 *
 * @SWG\Definition(definition="CarDto")
 * @package app\dto
 */
class CarDto
{
    use \app\dto\LoadFromArray;

    /**
     * Количество пройденных км
     * @SWG\Property()
     * @var integer
     */
    public $km;
    /**
     * Тип авто
     * @SWG\Property()
     * @var string
     */
    public $brand;
}