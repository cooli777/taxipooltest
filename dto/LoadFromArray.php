<?php
/**
 * Created by PhpStorm.
 * User: cooli777
 * Date: 06.04.17
 * Time: 17:10
 */

namespace app\dto;


trait LoadFromArray
{
    /**
     * Наполнение объекта
     *
     * @param array $data Данные
     * @return static
     */
    public static function loadFromArray(array $data)
    {
        $self = new static();
        foreach ($data as $propertyName => $propertyValue) {
            if (!property_exists($self, $propertyName)) {
                continue;
            }

            $self->$propertyName = $propertyValue;
        }

        return $self;
    }
}