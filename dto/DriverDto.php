<?php

namespace app\dto;

/**
 * Class DriverDto
 *
 * @SWG\Definition(definition="DriverDto")
 * @package app\dto
 */
class DriverDto
{
    use \app\dto\LoadFromArray;

    /**
     * Тип квалификации водителя
     * @SWG\Property()
     * @var string
     */
    public $type;
}