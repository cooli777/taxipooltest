<?php

namespace app\dto;

/**
 * Class ParkDto
 *
 * @SWG\Definition(definition="ParkDto")
 * @package app\dto
 */
class ParkDto
{
    use \app\dto\LoadFromArray;

    /**
     * Количество парковочных мест
     * @SWG\Property()
     * @var integer
     */
    public $places;
}