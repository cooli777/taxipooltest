<?php
/**
 * Created by PhpStorm.
 * User: johnbrown
 * Date: 20.07.2018
 * Time: 12:06
 */

namespace app\dto;


use app\entity\report\ReportDaily;
use app\entity\report\ReportMonthly;

class ReportDto
{
    /** @var ReportMonthly */
    public $reportAll;
    /** @var ReportMonthly[] */
    public $reportsMonthly;
    /** @var ReportDaily[] */
    public $reportsDaily;

    /**
     * ReportDto constructor.
     *
     * @param ReportMonthly   $reportAll
     * @param ReportMonthly[] $reportsMonthly
     * @param ReportDaily[]   $reportsDaily
     */
    public function __construct(ReportMonthly $reportAll, array $reportsMonthly, array $reportsDaily)
    {
        $this->reportAll      = $reportAll;
        $this->reportsMonthly = $reportsMonthly;
        $this->reportsDaily   = $reportsDaily;
    }

}