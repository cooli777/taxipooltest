<?php

namespace app\entity\driver;


use Generator;

class DriverCollection
{
    /** @var Driver[] */
    private $drivers;
    /** @var Generator */
    private $generator;

    /**
     * DriverCollection constructor.
     */
    public function __construct()
    {
        $this->drivers = [];
    }

    /**
     * @param Driver $driver
     */
    public function addDriver(Driver $driver)
    {
        $this->drivers[] = $driver;
    }

    /**
     * @return Driver[]
     */
    public function getDrivers(): array
    {
        return $this->drivers;
    }

    /**
     * @param Driver[] $drivers
     */
    public function setDrivers(array $drivers)
    {
        $this->drivers = [];
        array_walk($drivers, function($driver){
                $this->addDriver($driver);
            }
        );
    }

    /**
     * Получение генератора
     *
     * @return Generator
     */
    private function getGenerator()
    {
        foreach($this->drivers as $driver) {
            yield $driver;
        }
    }

    /**
     * обновляет generator, указатель смотрит на нулевой элемент
     */
    public function renewGenerator()
    {
        $this->generator = $this->getGenerator();
    }

    /**
     * Получение следуещего водителя из генератора
     *
     * @return Driver|null
     */
    public function getNextValue()
    {
        $value = $this->generator->current();
        $this->generator->next();

        return $value;
    }
}