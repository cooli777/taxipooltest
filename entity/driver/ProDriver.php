<?php

namespace app\entity\driver;


class ProDriver extends Driver
{
    const TYPE = 'pro';
    const ORDER_COEFFICIENT = 1.3;
    const ECONOMICAL_COEFFICIENT = 0.8;
}