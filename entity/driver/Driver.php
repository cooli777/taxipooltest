<?php
/**
 * Created by PhpStorm.
 * User: johnbrown
 * Date: 18.07.2018
 * Time: 18:18
 */

namespace app\entity\driver;


use app\dto\DriverDto;

abstract class Driver
{
    const TYPE = 'driver';
    const ORDER_COEFFICIENT = 1.0;
    const ECONOMICAL_COEFFICIENT = 1.0;

    /**
     * Возвращает коэффициент экономичности
     * @return float
     */
    public function getEconomicalCoefficient(): float
    {
        return static::ECONOMICAL_COEFFICIENT;
    }

    /**
     * Возвращает коэффициент выполненных заказов
     * @return float
     */
    public function getOrderCoefficient(): float
    {
        return static::ORDER_COEFFICIENT;
    }

    /**
     * Возвраает тип водителя
     *
     * @return string
     */
    public function getType(): string
    {
        return static::TYPE;
    }

    /**
     * Присвоение свойствам обьекта значений из Dto
     *
     * @param DriverDto $driverDto
     */
    public function setPropertyFromDto(DriverDto $driverDto)
    {
        foreach($this as $key => $_) {
            if(!isset($driverDto->$key)){
                continue;
            }
            $this->$key = $driverDto->$key;
        }
    }
}