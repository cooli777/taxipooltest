<?php

namespace app\entity\report;


use app\factories\ReportFactory;
use app\utils\AddDataFromReportDailyToReportMonthly;
use app\utils\DateHelper;
use DateTime;

class ReportMonthlyCollection
{
    /** @var ReportMonthly[] */
    private $reports;
    /** @var DateHelper */
    private $dateHelper;
    /** @var ReportFactory */
    private $reportFactory;
    /** @var AddDataFromReportDailyToReportMonthly */
    private $addDataFromReportDailyToReportMonthly;

    /**
     * ReportDailyCollection constructor.
     *
     * @param DateHelper                            $dateHelper
     * @param ReportFactory                         $reportFactory
     * @param AddDataFromReportDailyToReportMonthly $addDataFromReportDailyToReportMonthly
     */
    public function __construct(
        DateHelper $dateHelper,
        ReportFactory $reportFactory,
        AddDataFromReportDailyToReportMonthly $addDataFromReportDailyToReportMonthly
    )
    {
        $this->dateHelper                            = $dateHelper;
        $this->reportFactory                         = $reportFactory;
        $this->addDataFromReportDailyToReportMonthly = $addDataFromReportDailyToReportMonthly;
        $this->reports                               = [];
    }

    /**
     * Добавить данные в месячный отчет из дневного
     *
     * @param ReportDaily $reportDaily
     * @param DateTime    $dateReport
     */
    public function addDataToReportMontly(ReportDaily $reportDaily, DateTime $dateReport)
    {
        $monthlyReport = $this->getMonthlyReport($dateReport);
        $this->addDataFromReportDailyToReportMonthly->addDataToReport($monthlyReport, $reportDaily);
    }


    /**
     * Получить имеющийся или создать новый месячный отчет
     *
     * @param DateTime $dateReport
     *
     * @return ReportMonthly|mixed|null
     */
    private function getMonthlyReport(DateTime $dateReport)
    {
        $monthlyReport    = null;
        $dateReportString = $this->getDateString($dateReport);
        if (isset($this->reports[ $dateReportString ])) {
            $monthlyReport = $this->reports[ $dateReportString ];
        } else {
            $monthlyReport                      = $this->reportFactory->createReportMonthly();
            $this->reports[ $dateReportString ] = $monthlyReport;
        }

        return $monthlyReport;
    }

    /**
     * Получить ключ для этого месяца
     *
     * @param DateTime $dateReport
     *
     * @return string
     */
    private function getDateString(DateTime $dateReport)
    {
        return $this->dateHelper->getDateYearMonth($dateReport);
    }

    /**
     * @return ReportMonthly[]
     */
    public function getReports(): array
    {
        return $this->reports;
    }
}