<?php
/**
 * Created by PhpStorm.
 * User: johnbrown
 * Date: 20.07.2018
 * Time: 0:20
 */

namespace app\entity\report;


use app\utils\DateHelper;
use DateTime;

class ReportDailyCollection
{
    /** @var ReportDaily[] */
    private $reports;
    /** @var DateHelper */
    private $dateHelper;

    /**
     * ReportDailyCollection constructor.
     *
     * @param DateHelper $dateHelper
     */
    public function __construct(DateHelper $dateHelper)
    {
        $this->dateHelper = $dateHelper;
        $this->reports = [];
    }

    public function addReport(ReportDaily $reportDaily, DateTime $dateReport)
    {
        $this->reports[$this->getDateString($dateReport)][] = $reportDaily;
    }

    private function getDateString(DateTime $dateReport)
    {
        return $this->dateHelper->getDateYmd($dateReport);
    }

    /**
     * @return ReportDaily[]
     */
    public function getReports(): array
    {
        return $this->reports;
    }
}