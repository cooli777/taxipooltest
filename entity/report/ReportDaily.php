<?php
/**
 * Created by PhpStorm.
 * User: johnbrown
 * Date: 19.07.2018
 * Time: 15:30
 */

namespace app\entity\report;


class ReportDaily
{
    /** @var boolean */
    public $haveCar;
    /** @var boolean */
    public $haveDriver;
    /** @var string */
    public $carBrand;
    /** @var boolean */
    public $carIsBroken;
    /** @var boolean */
    public $carIsBrokenToday;
    /** @var float */
    public $probabilityFailure;
    /** @var string */
    public $typeDriver;
    /** @var int */
    public $carCountTraveledKmToday;
    /** @var int */
    public $carCountTraveledKmAll;
    /** @var int */
    public $countOrder;
    /** @var float */
    public $fuelConsumption;

    /**
     * ReportDaily constructor.
     *
     * @param bool $haveCar
     * @param bool $haveDriver
     */
    public function __construct(bool $haveCar = true, bool $haveDriver = true)
    {
        $this->haveCar                 = $haveCar;
        $this->haveDriver              = $haveDriver;
        $this->carIsBroken             = false;
        $this->carIsBrokenToday        = false;
        $this->carCountTraveledKmToday = 0;
        $this->carCountTraveledKmAll   = 0;
        $this->countOrder              = 0;
        $this->fuelConsumption         = 0.0;
    }
}