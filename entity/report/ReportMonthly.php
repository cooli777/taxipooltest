<?php

namespace app\entity\report;


class ReportMonthly
{
    /** @var int */
    public $countCasesCarWithoutDriver;
    /** @var int */
    public $countCasesDriverWithoutCar;
    /** @var int */
    public $countBrokenCars;
    /** @var int */
    public $countDaysBrokenCars;
    /** @var int */
    public $countBrokenCarsLuda;
    /** @var int */
    public $countBrokenCarsHomba;
    /** @var int */
    public $countBrokenCarsHendai;
    /** @var int */
    public $countCarOnRaceProbabilityFailureMore50Less75;
    /** @var int */
    public $countCarOnRaceProbabilityFailureMore75Less100;
    /** @var int */
    public $countCarOnRaceProbabilityFailure100;
    /** @var int */
    public $countRouteCarsLuda;
    /** @var int */
    public $countRouteCarsHomba;
    /** @var int */
    public $countRouteCarsHendai;
    /** @var int */
    public $countRouteDriverDefault;
    /** @var int */
    public $countRouteDriverPro;
    /** @var int */
    public $carCountTraveledKm;
    /** @var int */
    public $countOrder;
    /** @var float */
    public $fuelConsumption;

    /**
     * ReportMonthly constructor.
     */
    public function __construct()
    {
        $this->countCasesCarWithoutDriver                    = 0;
        $this->countCasesDriverWithoutCar                    = 0;
        $this->countBrokenCars                               = 0;
        $this->countDaysBrokenCars                           = 0;
        $this->countBrokenCarsLuda                           = 0;
        $this->countBrokenCarsHomba                          = 0;
        $this->countBrokenCarsHendai                         = 0;
        $this->countCarOnRaceProbabilityFailureMore50Less75  = 0;
        $this->countCarOnRaceProbabilityFailureMore75Less100 = 0;
        $this->countCarOnRaceProbabilityFailure100           = 0;
        $this->countRouteCarsLuda                            = 0;
        $this->countRouteCarsHomba                           = 0;
        $this->countRouteCarsHendai                          = 0;
        $this->countRouteDriverDefault                       = 0;
        $this->countRouteDriverPro                           = 0;
        $this->carCountTraveledKm                            = 0;
        $this->countOrder                                    = 0;
        $this->fuelConsumption                               = 0.0;
    }

}