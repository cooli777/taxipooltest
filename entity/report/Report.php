<?php

namespace app\entity\report;


use app\factories\ReportFactory;
use DateTime;

class Report
{
    /** @var ReportDailyCollection */
    private $reportDailyCollection;
    /** @var ReportMonthlyCollection */
    private $reportMonthlyCollection;
    /** @var ReportFactory */
    private $reportFactory;

    /**
     * Report constructor.
     *
     * @param ReportDailyCollection   $reportDailyCollection
     * @param ReportMonthlyCollection $reportMonthlyCollection
     * @param ReportFactory           $reportFactory
     */
    public function __construct(
        ReportDailyCollection $reportDailyCollection,
        ReportMonthlyCollection $reportMonthlyCollection,
        ReportFactory $reportFactory
    )
    {
        $this->reportDailyCollection = $reportDailyCollection;
        $this->reportMonthlyCollection = $reportMonthlyCollection;
        $this->reportFactory = $reportFactory;
    }

    public function addReport(ReportDaily $reportDaily, DateTime $currentDate)
    {
        $this->reportDailyCollection->addReport($reportDaily, $currentDate);
        $this->reportMonthlyCollection->addDataToReportMontly($reportDaily, $currentDate);
    }

    public function getReportDto()
    {
        return $this->reportFactory->createReportDto($this->reportMonthlyCollection->getReports(), $this->reportDailyCollection->getReports());
    }
}