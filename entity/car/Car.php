<?php

namespace app\entity\car;


use app\dto\CarDto;

/**
 * Class Car
 *
 * @SWG\Definition(definition="Car")
 * @package app\entity
 */
abstract class Car
{
    const BRAND = 'Car';
    const ECONOMICAL_COEFFICIENT = 1.0;
    const BREAKDOWN_RATIO = 1.0;

    /**
     * Количество пройденных км
     * @SWG\Property()
     * @var integer
     */
    private $km;

    /**
     * Дата поломки авто
     * @SWG\Property()
     * @var \DateTime|null
     */
    private $brokenAt;

    /**
     * Возвращает коэффициент экономичности
     * @return float
     */
    public function getEconomicalCoefficient(): float
    {
        return static::ECONOMICAL_COEFFICIENT;
    }

    /**
     * Возвращает коэффициент возможности поломки
     * @return float
     */
    public function getBreakdownRatio(): float
    {
        return static::BREAKDOWN_RATIO;
    }

    /**
     * @return \DateTime|null
     */
    public function getBrokenAt()
    {
        return $this->brokenAt;
    }

    /**
     * @param \DateTime|null $brokenAt
     */
    public function setBrokenAt(\DateTime $brokenAt = null)
    {
        if($brokenAt !== null){
            $this->brokenAt = clone $brokenAt;
        }else{
            $this->brokenAt = $brokenAt;
        }
    }

    /**
     * @return int
     */
    public function getKm(): int
    {
        return $this->km;
    }

    /**
     * Увеличение пройденной дистанции
     *
     * @param int $km
     *
     * @return int
     */
    public function addKm(int $km): int
    {
        return $this->km += $km;
    }

    /**
     * Возвращает бренд
     *
     * @return string
     */
    public function getBrand(): string
    {
        return static::BRAND;
    }

    /**
     * Присвоение свойствам обьекта значений из Dto
     *
     * @param CarDto $carDto
     */
    public function setPropertyFromDto(CarDto $carDto)
    {
        foreach($this as $key => $_) {
            if(!isset($carDto->$key)){
                continue;
            }
            $this->$key = $carDto->$key;
        }
    }
}