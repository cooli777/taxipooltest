<?php

namespace app\entity\car;


use Generator;

class CarCollection
{
    /** @var Car[] */
    private $cars;
    /** @var Generator */
    private $generator;

    /**
     * CarCollection constructor.
     */
    public function __construct()
    {
        $this->cars = [];
    }

    /**
     * @param Car $car
     */
    public function addCar(Car $car)
    {
        $this->cars[] = $car;
    }

    /**
     * @return Car[]
     */
    public function getCars(): array
    {
        return $this->cars;
    }

    /**
     * @param Car[] $cars
     */
    public function setCars(array $cars)
    {
        $this->cars = [];
        array_walk($cars, function($car){
                $this->addCar($car);
            }
        );
    }

    /**
     * Получение генератора
     *
     * @return Generator
     */
    private function getGenerator()
    {
        foreach($this->cars as $car) {
            yield $car;
        }
    }

    /**
     * обновляет generator, указатель смотрит на нулевой элемент
     */
    public function renewGenerator()
    {
        $this->generator = $this->getGenerator();
    }

    /**
     * Получение следуещего авто из генератора
     *
     * @return Car|null
     */
    public function getNextValue()
    {
        $value = $this->generator->current();
        $this->generator->next();

        return $value;
    }

    /**
     * Количество авто
     *
     * @return int
     */
    public function getCountCars(): int
    {
        return count($this->cars);
    }
}