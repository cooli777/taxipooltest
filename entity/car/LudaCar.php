<?php

namespace app\entity\car;


class LudaCar extends Car
{
    const BRAND = 'luda';
    const BREAKDOWN_RATIO = 3.0;
}