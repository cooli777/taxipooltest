<?php
/**
 * Created by PhpStorm.
 * User: johnbrown
 * Date: 18.07.2018
 * Time: 17:36
 */

namespace app\entity\car;


class HombaCar extends Car
{
    const BRAND = 'homba';
    const ECONOMICAL_COEFFICIENT = 0.7;
}