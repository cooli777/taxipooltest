<?php

namespace app\entity;

use app\entity\car\CarCollection;
use app\entity\driver\DriverCollection;

class TaxiPool
{
    /**
     * Количество парковочных мест
     * @var integer
     */
    private $places;
    /** @var DriverCollection */
    private $driverCollection;
    /** @var CarCollection */
    private $carCollection;
    /** @var int */
    private $simulationCountDays;

    /**
     * TaxiPoolDto constructor.
     *
     * @param DriverCollection $driverCollection
     * @param CarCollection    $carCollection
     */
    public function __construct(
        DriverCollection $driverCollection,
        CarCollection $carCollection
    )
    {
        $this->places           = null;
        $this->driverCollection = $driverCollection;
        $this->carCollection    = $carCollection;
    }

    /**
     * @return CarCollection
     */
    public function getCarCollection()
    {
        return $this->carCollection;
    }

    /**
     * @return DriverCollection
     */
    public function getDriverCollection()
    {
        return $this->driverCollection;
    }

    /**
     * @param int $places
     */
    public function setPlaces(int $places)
    {
        $this->places = $places;
    }

    /**
     * @return int
     */
    public function getPlaces(): int
    {
        return $this->places;
    }

    /**
     * @return int
     */
    public function getSimulationCountDays(): int
    {
        return $this->simulationCountDays;
    }

    /**
     * @param int $simulationCountDays
     */
    public function setSimulationCountDays(int $simulationCountDays)
    {
        $this->simulationCountDays = $simulationCountDays;
    }
}