<?php

namespace app\controllers;


use app\facades\TaxiPoolSimulationWorkFacade;
use app\factories\TaxiPoolFactory;
use app\managers\ReportManager;

class TaxiPoolController extends BaseController
{
    /** @var TaxiPoolFactory */
    private $taxiPoolFactory;
    /** @var TaxiPoolSimulationWorkFacade */
    private $taxiPoolSimulationWorkFacade;
    /** @var ReportManager */
    private $reportManager;

    /**
     * AuthController constructor.
     *
     * @param string                       $id
     * @param \yii\base\Module             $module
     * @param TaxiPoolFactory              $taxiPoolFactory
     * @param TaxiPoolSimulationWorkFacade $taxiPoolSimulationWorkFacade
     * @param ReportManager                $reportManager
     * @param array                        $config
     */
    public function __construct(
        $id,
        $module,
        TaxiPoolFactory $taxiPoolFactory,
        TaxiPoolSimulationWorkFacade $taxiPoolSimulationWorkFacade,
        ReportManager $reportManager,
        array $config = []
    )
    {
        $this->taxiPoolFactory              = $taxiPoolFactory;
        $this->taxiPoolSimulationWorkFacade = $taxiPoolSimulationWorkFacade;
        $this->reportManager                = $reportManager;
        parent::__construct($id, $module, $config);
    }

    /**
     * Displays homepage.
     *
     * @return string
     *
     * @throws \app\exceptions\car\CarInvalidBrandException
     * @throws \app\exceptions\driver\DriverInvalidTypeException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     * @throws \app\exceptions\taxipool\TaxiPoolInvalidDataException
     */
    public function actionIndex()
    {
        $data       = $this->getDataFromJson();
        $taxiPool   = $this->taxiPoolFactory->createTaxiPool($data);
        $reportDto  = $this->taxiPoolSimulationWorkFacade->exec($taxiPool);
        $pathToFile = $this->reportManager->createReport($reportDto);
        \Yii::$app->response->sendFile($pathToFile,
                                       "Отчет о работе таксопарка.doc",
                                       [
                                           'Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                                           'Content-Type: application/octet-stream',
                                           'Content-Description: File Transfer',
                                           'Content-Disposition: attachment; filename="Отчет о работе таксопарка.doc"',
                                           'Expires: 0',
                                           'Content-Length: ' . filesize($pathToFile),
                                       ]
        );

        unlink($pathToFile);

        return;
    }
}
