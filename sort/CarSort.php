<?php
/**
 * Created by PhpStorm.
 * User: johnbrown
 * Date: 19.07.2018
 * Time: 16:31
 */

namespace app\sort;


use app\entity\car\Car;
use app\entity\car\HendaiCar;
use app\entity\car\HombaCar;
use app\entity\car\LudaCar;

class CarSort
{
    /**
     * @param Car[] $cars
     *
     * @return Car[]
     */
    public function sortByBrand(array $cars)
    {
        uasort($cars, [ $this, 'getWeightByBrand' ]);

        return $cars;
    }

    /**
     * @param Car $car
     * @param Car $car1
     *
     * @return int
     */
    private function getWeightByBrand(Car $car, Car $car1)
    {
        $brand1 = $car->getBrand();
        $brand2 = $car1->getBrand();

        if($brand1 === $brand2){
            $weight = 0;
        }elseif ($brand1 > $brand2) {
            $weight = 1;
        }elseif ($brand1 < $brand2) {
            $weight = -1;
        }

        return $weight;
    }
}