<?php
/**
 * Created by PhpStorm.
 * User: johnbrown
 * Date: 19.07.2018
 * Time: 16:31
 */

namespace app\sort;


use app\entity\driver\DefaultDriver;
use app\entity\driver\Driver;
use app\entity\driver\ProDriver;

class DriverSort
{
    /**
     * @param Driver[] $driver
     *
     * @return Driver[]
     */
    public function sortByType(array $driver)
    {
        uasort($driver, [ $this, 'getWeightByType']);
        return $driver;
    }

    private function getWeightByType(Driver $driver)
    {
        $weight = 0;
        switch($driver->getType()){
            case ProDriver::TYPE:
                $weight = 0;
                break;
            case DefaultDriver::TYPE:
                $weight = 1;
                break;
        }

        return $weight;
    }
}