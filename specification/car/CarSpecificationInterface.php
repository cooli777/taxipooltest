<?php

namespace app\specification\car;


use app\dto\CarDto;

interface CarSpecificationInterface
{
    public function isSatisfiedBy(CarDto $carDto);
}