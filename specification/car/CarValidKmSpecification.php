<?php

namespace app\specification\car;


use app\dto\CarDto;
use app\entity\car\HendaiCar;
use app\entity\car\HombaCar;
use app\entity\car\LudaCar;
use app\exceptions\car\CarInvalidBrandException;
use app\exceptions\car\CarInvalidKmException;

class CarValidKmSpecification implements CarSpecificationInterface
{
    /**
     * @param CarDto $carDto
     *
     * @throws CarInvalidKmException
     */
    public function isSatisfiedBy(CarDto $carDto)
    {
        if (!is_int($carDto->km)){
            throw new CarInvalidKmException($carDto->km);
        }
    }
}