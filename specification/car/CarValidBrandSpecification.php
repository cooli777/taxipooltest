<?php

namespace app\specification\car;


use app\dto\CarDto;
use app\entity\car\HendaiCar;
use app\entity\car\HombaCar;
use app\entity\car\LudaCar;
use app\exceptions\car\CarInvalidBrandException;

class CarValidBrandSpecification implements CarSpecificationInterface
{
    /**
     * @param CarDto $carDto
     *
     * @throws CarInvalidBrandException
     */
    public function isSatisfiedBy(CarDto $carDto)
    {
        if(!is_string($carDto->brand)){
            throw new CarInvalidBrandException();
        }

        $brand = mb_strtolower($carDto->brand);
        if ($brand !== mb_strtolower(LudaCar::BRAND) && $brand !== mb_strtolower(HombaCar::BRAND) && $brand !== mb_strtolower(HendaiCar::BRAND) ){
            throw new CarInvalidBrandException($carDto->brand);
        }
    }
}