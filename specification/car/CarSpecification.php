<?php

namespace app\specification\car;


use app\dto\CarDto;
use app\exceptions\car\CarInvalidDataException;
use app\exceptions\DisplayWebException;

class CarSpecification implements CarSpecificationInterface
{
    /** @var CarSpecificationInterface */
    private $specifications;

    /**
     * CarSpecification constructor.
     *
     * @param CarSpecificationInterface ...$specifications
     */
    public function __construct(CarSpecificationInterface ...$specifications)
    {
        $this->specifications = $specifications;
    }

    /**
     * @param CarDto $carDto
     *
     * @throws CarInvalidDataException
     */
    public function isSatisfiedBy(CarDto $carDto)
    {
        $errors = [];
        foreach($this->specifications as $specification) {
            try {
                $specification->isSatisfiedBy($carDto);
            } catch (DisplayWebException $e) {
                $errors[] = $e->getProperties();
            }
        }
        if (count($errors) > 0) {
            throw new CarInvalidDataException($errors);
        }
    }
}