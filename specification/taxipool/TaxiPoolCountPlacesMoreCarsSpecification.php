<?php

namespace app\specification\taxipool;


use app\entity\TaxiPool;
use app\exceptions\taxipool\TaxiPoolPlacesLessException;

class TaxiPoolCountPlacesMoreCarsSpecification implements TaxiPoolSpecificationInterface
{
    /**
     * @param TaxiPool $taxiPool
     *
     * @throws TaxiPoolPlacesLessException
     */
    public function isSatisfiedBy(TaxiPool $taxiPool)
    {
        $countCars = $taxiPool->getCarCollection()->getCountCars();
        if ($taxiPool->getPlaces() < $countCars){
            throw new TaxiPoolPlacesLessException($countCars);
        }
    }
}