<?php
/**
 * Created by PhpStorm.
 * User: johnbrown
 * Date: 20.07.2018
 * Time: 13:19
 */

namespace app\specification\taxipool;


use app\entity\TaxiPool;
use app\exceptions\DisplayWebException;
use app\exceptions\taxipool\TaxiPoolInvalidDataException;

class TaxiPoolSpecification implements TaxiPoolSpecificationInterface
{
    /** @var TaxiPoolSpecificationInterface */
    private $specifications;

    public function __construct(TaxiPoolSpecificationInterface ...$specifications)
    {
        $this->specifications = $specifications;
    }

    /**
     * @param TaxiPool $taxiPool
     *
     * @throws TaxiPoolInvalidDataException
     */
    public function isSatisfiedBy(TaxiPool $taxiPool)
    {
        $errors = [];
        foreach($this->specifications as $specification) {
            try {
                $specification->isSatisfiedBy($taxiPool);
            } catch (DisplayWebException $e) {
                $errors[] = $e->getProperties();
            }
        }
        if (count($errors) > 0) {
            throw new TaxiPoolInvalidDataException($errors);
        }
    }
}