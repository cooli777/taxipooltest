<?php

namespace app\specification\taxipool;


use app\entity\TaxiPool;

interface TaxiPoolSpecificationInterface
{
    public function isSatisfiedBy(TaxiPool $item);
}