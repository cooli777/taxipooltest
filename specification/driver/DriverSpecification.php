<?php
/**
 * Created by PhpStorm.
 * User: johnbrown
 * Date: 20.07.2018
 * Time: 14:30
 */

namespace app\specification\driver;


use app\dto\DriverDto;
use app\exceptions\DisplayWebException;
use app\exceptions\driver\DriverInvalidDataException;

class DriverSpecification implements DriverSpecificationInterface
{
    /** @var DriverSpecificationInterface */
    private $specifications;

    public function __construct(DriverSpecificationInterface ...$specifications)
    {
        $this->specifications = $specifications;
    }

    /**
     * @param DriverDto $driverDto
     *
     * @throws DriverInvalidDataException
     */
    public function isSatisfiedBy(DriverDto $driverDto)
    {
        $errors = [];
        foreach($this->specifications as $specification) {
            try {
                $specification->isSatisfiedBy($driverDto);
            } catch (DisplayWebException $e) {
                $errors[] = $e->getProperties();
            }
        }
        if (count($errors) > 0) {
            throw new DriverInvalidDataException($errors);
        }
    }
}