<?php

namespace app\specification\driver;


use app\dto\DriverDto;
use app\entity\driver\DefaultDriver;
use app\entity\driver\ProDriver;
use app\exceptions\driver\DriverInvalidTypeException;

class DriverValidTypeSpecification implements DriverSpecificationInterface
{
    /**
     * @param DriverDto $driverDto
     *
     * @throws DriverInvalidTypeException
     */
    public function isSatisfiedBy(DriverDto $driverDto)
    {
        if(!is_string($driverDto->type)){
            throw new DriverInvalidTypeException();
        }

        $type = mb_strtolower($driverDto->type);
        if ($type !== mb_strtolower(ProDriver::TYPE) && $type !== mb_strtolower(DefaultDriver::TYPE) ){
            throw new DriverInvalidTypeException($driverDto->type);
        }
    }
}