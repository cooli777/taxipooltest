<?php
/**
 * Created by PhpStorm.
 * User: johnbrown
 * Date: 20.07.2018
 * Time: 14:29
 */

namespace app\specification\driver;


use app\dto\DriverDto;

interface DriverSpecificationInterface
{
    public function isSatisfiedBy(DriverDto $driverDto);
}