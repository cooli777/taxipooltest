<?php

namespace app\factories;


use app\dto\ReportDto;
use app\entity\car\Car;
use app\entity\driver\Driver;
use app\entity\report\ReportDaily;
use app\entity\report\ReportMonthly;
use app\managers\CarManager;

class ReportFactory
{
    /** @var CarManager */
    private $carManager;

    /**
     * ReportFactory constructor.
     *
     * @param CarManager $carManager
     */
    public function __construct(CarManager $carManager)
    {
        $this->carManager = $carManager;
    }

    /**
     * Создание дневного отчета когда есть авто и водитель
     *
     * @param Car    $car
     * @param Driver $driver
     *
     * @return ReportDaily
     */
    public function createForCarAndDriver(Car $car, Driver $driver): ReportDaily
    {
        $reportDaily             = new ReportDaily();
        $reportDaily->carBrand   = $car->getBrand();
        $reportDaily->typeDriver = $driver->getType();

        return $reportDaily;
    }

    /**
     * Создание дневного отчета когда авто сломан
     *
     * @param Car $car
     *
     * @return ReportDaily
     */
    public function createCarBroken(Car $car): ReportDaily
    {
        $reportDaily             = new ReportDaily(true, false);
        $reportDaily->carBrand   = $car->getBrand();
        $reportDaily->carCountTraveledKmAll = $car->getKm();
        $reportDaily->carIsBroken = true;
        $reportDaily->probabilityFailure = $this->carManager->getBreakdownRatio($car);
        return $reportDaily;
    }

    /**
     * Создание дневного отчета когда авто не нашлось свободного водителя
     *
     * @param Car $car
     *
     * @return ReportDaily
     */
    public function createCarWithoutDriver(Car $car): ReportDaily
    {
        $reportDaily             = new ReportDaily(true, false);
        $reportDaily->carBrand   = $car->getBrand();
        $reportDaily->carCountTraveledKmAll = $car->getKm();
        $reportDaily->probabilityFailure = $this->carManager->getBreakdownRatio($car);
        return $reportDaily;
    }

    /**
     * Создание дневного отчета когда водителю не нашлось свободного авто
     *
     * @param Car $driver
     *
     * @return ReportDaily
     */
    public function createDriverWithoutCar(Driver $driver): ReportDaily
    {
        $reportDaily             = new ReportDaily(false, true);
        $reportDaily->typeDriver = $driver->getType();
        return $reportDaily;
    }

    /**
     * Создание ежемесячного отчета
     *
     * @return ReportMonthly
     */
    public function createReportMonthly()
    {
        return new ReportMonthly();
    }

    /**
     * @param ReportMonthly[] $reportsMonthly
     *
     * @return ReportMonthly
     */
    public function createReportAll(array $reportsMonthly)
    {
        $reportAll = $this->createReportMonthly();
        foreach($reportsMonthly as $reportMonthly){
            foreach($reportMonthly as $key => $value){
                $reportAll->$key += $value;
            }
        }
        return $reportAll;
    }

    /**
     * @param array $reportsMonthly
     * @param array $reportsDaily
     *
     * @return ReportDto
     */
    public function createReportDto(array $reportsMonthly, array $reportsDaily)
    {
        return new ReportDto($this->createReportAll($reportsMonthly), $reportsMonthly, $reportsDaily);
    }
}