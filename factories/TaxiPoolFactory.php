<?php

namespace app\factories;

use app\dto\ParkDto;
use app\entity\car\CarCollection;
use app\entity\driver\DriverCollection;
use app\entity\TaxiPool;
use app\sort\CarSort;
use app\sort\DriverSort;
use app\specification\taxipool\TaxiPoolSpecification;

/**
 * Class TaxiPoolFactory
 *
 * @package app\factories
 */
class TaxiPoolFactory
{

    /** @var CarFactory */
    private $carFactory;
    /** @var DriverFactory */
    private $driverFactory;
    /** @var CarSort */
    private $carSort;
    /** @var DriverSort */
    private $driverSort;
    /** @var TaxiPoolSpecification */
    private $taxiPoolSpecification;

    /**
     * TaxiPoolFactory constructor.
     *
     * @param CarFactory            $carFactory
     * @param DriverFactory         $driverFactory
     * @param CarSort               $carSort
     * @param DriverSort            $driverSort
     * @param TaxiPoolSpecification $taxiPoolSpecification
     */
    public function __construct(
        CarFactory $carFactory,
        DriverFactory $driverFactory,
        CarSort $carSort,
        DriverSort $driverSort,
        TaxiPoolSpecification $taxiPoolSpecification
    )
    {
        $this->carFactory    = $carFactory;
        $this->driverFactory = $driverFactory;
        $this->carSort       = $carSort;
        $this->driverSort    = $driverSort;
        $this->taxiPoolSpecification = $taxiPoolSpecification;
    }

    /**
     * @param array $dataRaw
     *
     * @return TaxiPool
     * @throws \app\exceptions\car\CarInvalidBrandException
     * @throws \app\exceptions\driver\DriverInvalidTypeException
     * @throws \app\exceptions\taxipool\TaxiPoolInvalidDataException
     */
    public function createTaxiPool(array $dataRaw): TaxiPool
    {
        //todo добавить спецификации в Dto для проверки правил
        //перехватывать exceptions
        $taxiPool = new TaxiPool(new DriverCollection(), new CarCollection());
        $this->setParkDto($taxiPool, $dataRaw);
        $this->setDriversDto($taxiPool, $dataRaw);
        $this->setCarsDto($taxiPool, $dataRaw);
        $this->setSimulationCountDays($taxiPool, $dataRaw);
        $this->sortData($taxiPool);
        $this->taxiPoolSpecification->isSatisfiedBy($taxiPool);

        return $taxiPool;
    }

    /**
     * Сортировка сущностей
     *
     * @param TaxiPool $taxiPool
     */
    private function sortData(TaxiPool $taxiPool)
    {
        $taxiPool->getCarCollection()->setCars(
            $this->carSort->sortByBrand($taxiPool->getCarCollection()->getCars())
        );
        $taxiPool->getDriverCollection()->setDrivers(
            $this->driverSort->sortByType($taxiPool->getDriverCollection()->getDrivers())
        );
    }

    /**
     * @param TaxiPool $taxiPool
     * @param array    $dataRaw
     */
    private function setSimulationCountDays(TaxiPool $taxiPool, array $dataRaw)
    {
        if (isset($dataRaw[ 'simulationCountDays' ])) {
            $taxiPool->setSimulationCountDays(
                $dataRaw[ 'simulationCountDays' ]
            );
        }
    }

    /**
     * @param TaxiPool $taxiPool
     * @param array    $dataRaw
     */
    private function setParkDto(TaxiPool $taxiPool, array $dataRaw)
    {
        if (isset($dataRaw[ 'park' ])) {
            $taxiPool->setPlaces(
                (ParkDto::loadFromArray($dataRaw[ 'park' ]))->places
            );
        }
    }

    /**
     * @param TaxiPool $taxiPool
     * @param array    $dataRaw
     *
     * @throws \app\exceptions\driver\DriverInvalidTypeException
     */
    private function setDriversDto(TaxiPool $taxiPool, array $dataRaw)
    {
        if (isset($dataRaw[ 'drivers' ]) && count($dataRaw[ 'drivers' ]) > 0) {
            foreach($dataRaw[ 'drivers' ] as $driver) {
                $taxiPool->getDriverCollection()->addDriver($this->driverFactory->createDriver($driver));
            }
        }
    }

    /**
     * @param TaxiPool $taxiPool
     * @param array    $dataRaw
     *
     * @throws \app\exceptions\car\CarInvalidBrandException
     * @throws \app\exceptions\car\CarInvalidDataException
     */
    private function setCarsDto(TaxiPool $taxiPool, array $dataRaw)
    {
        if (isset($dataRaw[ 'cars' ]) && count($dataRaw[ 'cars' ]) > 0) {
            foreach($dataRaw[ 'cars' ] as $car) {
                $taxiPool->getCarCollection()->addCar($this->carFactory->createCar($car));
            }
        }
    }

}