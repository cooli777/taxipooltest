<?php
/**
 * Created by PhpStorm.
 * User: johnbrown
 * Date: 18.07.2018
 * Time: 17:07
 */

namespace app\factories;


use app\dto\CarDto;
use app\entity\car\Car;
use app\entity\car\HendaiCar;
use app\entity\car\HombaCar;
use app\entity\car\LudaCar;
use app\exceptions\car\CarInvalidBrandException;
use app\specification\car\CarSpecification;

/**
 * Class CarFactory
 *
 * @package app\factories
 */
class CarFactory
{
    /** @var CarSpecification */
    private $carSpecification;

    /**
     * CarFactory constructor.
     *
     * @param CarSpecification $carSpecification
     */
    public function __construct(CarSpecification $carSpecification)
    {
        $this->carSpecification = $carSpecification;
    }

    /**
     * @param array $dataRaw
     *
     * @return Car
     * @throws CarInvalidBrandException
     * @throws \app\exceptions\car\CarInvalidDataException
     */
    public function createCar(array $dataRaw): Car
    {
        $carDto = $this->createCarDto($dataRaw);
        $car = $this->createCarEntityByBrand($carDto->brand);
        $car->setPropertyFromDto($carDto);

        return $car;
    }

    /**
     * @param array $dataRaw
     *
     * @return CarDto
     * @throws \app\exceptions\car\CarInvalidDataException
     */
    private function createCarDto(array $dataRaw): CarDto
    {
        $carDto = CarDto::loadFromArray($dataRaw);
        $this->carSpecification->isSatisfiedBy($carDto);
        return $carDto;
    }

    /**
     * Создание авто в зависимости от переданного бренда
     *
     * @param string $carBrandRaw
     *
     * @return Car
     * @throws CarInvalidBrandException
     */
    private function createCarEntityByBrand(string $carBrandRaw) : Car
    {
        $carBrand = mb_strtolower($carBrandRaw);
        switch($carBrand) {
            case HendaiCar::BRAND:
                $car = new HendaiCar();
                break;
            case HombaCar::BRAND:
                $car = new HombaCar();
                break;
            case LudaCar::BRAND:
                $car = new LudaCar();
                break;
            default:
                throw new CarInvalidBrandException($carBrandRaw);
                break;
        }

        return $car;
    }
}