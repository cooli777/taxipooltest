<?php
/**
 * Created by PhpStorm.
 * User: johnbrown
 * Date: 18.07.2018
 * Time: 17:07
 */

namespace app\factories;

use app\dto\DriverDto;
use app\entity\driver\DefaultDriver;
use app\entity\driver\Driver;
use app\entity\driver\ProDriver;
use app\exceptions\driver\DriverInvalidTypeException;
use app\specification\driver\DriverSpecification;


/**
 * Class DriverFactory
 *
 * @package app\factories
 */
class DriverFactory
{
    /** @var DriverSpecification */
    private $driverSpecification;

    /**
     * DriverFactory constructor.
     *
     * @param DriverSpecification $driverSpecification
     */
    public function __construct(DriverSpecification $driverSpecification)
    {
        $this->driverSpecification = $driverSpecification;
    }

    /**
     * @param array $dataRaw
     *
     * @return Driver
     * @throws DriverInvalidTypeException
     */
    public function createDriver(array $dataRaw): Driver
    {
        $driverDto = $this->createDriverDto($dataRaw);

        $driver    = $this->createDriverEntityByType($driverDto->type);
        $driver->setPropertyFromDto($driverDto);

        return $driver;
    }

    /**
     * @param array $dataRaw
     *
     * @return DriverDto
     * @throws \app\exceptions\driver\DriverInvalidDataException
     */
    private function createDriverDto(array $dataRaw)
    {
        $driverDto = DriverDto::loadFromArray($dataRaw);
        $this->driverSpecification->isSatisfiedBy($driverDto);
        return $driverDto;
    }

    /**
     * Создание водителя в зависимости от переданного типа
     *
     * @param string $driverTypeRaw
     *
     * @return Driver
     * @throws DriverInvalidTypeException
     */
    private function createDriverEntityByType(string $driverTypeRaw): Driver
    {
        $driverType = mb_strtolower($driverTypeRaw);
        switch($driverType) {
            case DefaultDriver::TYPE:
                $driver = new DefaultDriver();
                break;
            case ProDriver::TYPE:
                $driver = new ProDriver();
                break;
            default:
                throw new DriverInvalidTypeException($driverTypeRaw);
                break;
        }

        return $driver;
    }
}